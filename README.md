# fileDemo

#### 介绍
    打造开箱即用的基础框架。
    1、权限框架已封装好。开箱即用，本项目依赖的既是已经封装好的。
    2、todo...

#### 软件架构
    meta.api 控制台代码
    meta.app 小程序代码
    meta.clown 伪代码
    meta.common 基础功能
    meta.controller 彩票相关等一些测试的方法
    meta.ddd DDD领域驱动
    meta.kafka kafka相关
    meta.param param对象
    meta.scheduledInfo 定时任务



#### 安装教程

    https://blog.csdn.net/weixin_43555115/article/details/121167146

#### 使用说明

    最终解释权归作者所有。


