-- 用户表
CREATE TABLE `t_user` (
  `c_id` int(10)  NOT NULL AUTO_INCREMENT ,
  `C_USER_NAME` varchar(20) NOT NULL COMMENT '用户名',
  `C_PASSWORD` varchar(20)  NOT NULL COMMENT '密码',
  `C_EMAIL` varchar(20)  NOT NULL COMMENT '邮箱',
  `C_ADD_DATE` date DEFAULT NULL COMMENT '创建时间',
  `C_UPDATE_DATE` date DEFAULT NULL COMMENT '更新时间',
  `C_STATUS` INT(1) DEFAULT NULL COMMENT '用户状态',
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='/';


-- 初始化账号信息
insert into t_user (c_id,C_USER_NAME,C_PASSWORD,C_EMAIL,C_ADD_DATE,C_UPDATE_DATE,C_STATUS) values (1,'admin','123456','597925798@qq.com',NOW(),NOW(),3);
