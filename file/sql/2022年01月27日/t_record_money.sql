drop table if exists t_record_money;

CREATE TABLE `t_record_money`
(
    `C_ID`          bigint(11) NOT NULL AUTO_INCREMENT,
    `c_userId`      bigint(32)   DEFAULT NULL COMMENT '用户id',
    `c_type`        varchar(300) DEFAULT NULL COMMENT '钱的用途',
    `c_money`       varchar(300) DEFAULT NULL COMMENT '花了多少钱',
    `c_create_date` datetime     DEFAULT NULL COMMENT '生成时间',
    `c_update_date` datetime     DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`C_ID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
