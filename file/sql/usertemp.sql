
CREATE TABLE `t_user_tmp` (
  `c_id` int(10) COLLATE utf8_unicode_ci NOT NULL COMMENT '主键 - 业务自定义编号',
  `C_USER_NAME` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '用户名',
  `C_PASSWORD` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `C_EMAIL` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '邮箱',
  `C_ADD_DATE` date DEFAULT NULL COMMENT '创建时间',
  `C_UPDATE_DATE` date DEFAULT NULL COMMENT '更新时间',
  `C_STATUS` INT(1) DEFAULT NULL COMMENT '用户状态'

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户表临时表，防止死锁';
