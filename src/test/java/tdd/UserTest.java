package tdd;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.meta.Application;
import com.meta.common.vo.ResponseInfo;
import com.meta.ddd.dto.UserDTO;
import com.meta.ddd.entity.UserEntity;
import com.meta.ddd.feign.FeignClient;
import com.meta.ddd.param.UserParam;
import com.meta.ddd.repository.UserRepository;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 16:34
 * <p>
 * 测试驱动开发
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@Transactional
public class UserTest {

    @Autowired
    private UserRepository userRepository;


    @MockBean
    private FeignClient feignClient;

    private static UserParam userParam = new UserParam();

    static {
        userParam.setEmail("597925798@qq.com");
        userParam.setGender(1);
        userParam.setMobile("17612149663");
    }

    /**
     * 测试添加用户
     * 测试步骤
     * 1、制造假数据，param
     * 2、查询该数据是否存在，期望不存在
     * 3、执行添加用户操作
     * 4、查询该数据是否存在。期望存在
     */
    @Test
    public void addUserInfo() {

        Mockito.when(feignClient.sendMsg(Mockito.any())).thenReturn(ResponseInfo.succ());

        UserDTO userDTO = userRepository.queryByMobileNull(userParam.getEmail());
        assert userDTO == null;
        new UserEntity(userParam.getGender(), userParam.getMobile(), userParam.getEmail());
        userDTO = userRepository.queryByMobile(userParam.getEmail());
        assert userDTO != null;
    }

    /**
     * 测试添加用户
     * 测试步骤
     * 1、制造假数据，param
     * 2、查询该数据是否存在，期望不存在
     * 3、执行添加用户操作，且将手机号码置为空
     * 4、期望报错，手机号不对
     */
    @Test
    public void addUserInfoErrorMobile() {

        Mockito.when(feignClient.sendMsg(Mockito.any())).thenReturn(ResponseInfo.succ());
        userParam.setMobile(null);
        UserDTO userDTO = userRepository.queryByMobileNull(userParam.getEmail());
        assert userDTO == null;
        new UserEntity(userParam.getGender(), userParam.getMobile(), userParam.getEmail());
    }

    /**
     * 测试添加用户
     * 测试步骤
     * 1、制造假数据，param
     * 2、查询该数据是否存在，期望不存在
     * 3、执行添加用户操作，且将邮箱置为空
     * 4、期望报错，邮箱不对
     */
    @Test
    public void addUserInfoErrorEmail() {

        Mockito.when(feignClient.sendMsg(Mockito.any())).thenReturn(ResponseInfo.succ());
        userParam.setEmail(null);
        UserDTO userDTO = userRepository.queryByMobileNull(userParam.getEmail());
        assert userDTO == null;
        new UserEntity(userParam.getGender(), userParam.getMobile(), userParam.getEmail());
    }
}
