package xie;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.meta.Application;
import com.meta.api.param.UserNamePwdParam;
import com.meta.api.service.TransaInMethod;
import com.meta.api.service.UserServiceQuery;
import com.meta.api.service.UserServiceUpdate;
import com.meta.api.service.propagationService.TrancaPropagationA;
import java.util.concurrent.CountDownLatch;

/**
 * @Author: xieZW
 * @Date: 2021/11/16 16:37
 * <p>
 * 数据库死锁
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
//@Transactional   //这个注释掉会真的修改数据库中数据，这个放开后数据不会真的提交到数据库，为了直观测试数据是否回滚可以注释掉，当然也可使用assert来论证
public class DeadLockMysqlTest {

    @Autowired
    private UserServiceQuery userServiceQuery;
    @Autowired
    private UserServiceUpdate userServiceUpdate;
    @Autowired
    private TransaInMethod transaInMethod;
    @Autowired
    private TrancaPropagationA trancaPropagationA;


    static UserNamePwdParam userNamePwdParam = new UserNamePwdParam("小王", "123456");
    static UserNamePwdParam userNamePwdParamLogin = new UserNamePwdParam("admin", "123456");
    static UserNamePwdParam userNamePwdParamUpdate = new UserNamePwdParam("小王吧", "123456");


    /**
     * service加事务
     * Mapper里面加事务，里面两个方法， 都执行完然后报错
     * 期望：
     * mapper 1 修改成功
     * mapper 2 回滚
     */
    @Test
    public void fun26() {
        trancaPropagationA.update17();
    }


    /**
     * 提交时断网
     */
    @Test
    public void fun25() {
        trancaPropagationA.update16();
    }

    /**
     * 测试是否报连接池异常
     */
    @Test
    public void fun24() {

        //发枪器
        final CountDownLatch latch = new CountDownLatch(20);

        for (int i = 0; i < 20; i++) {
            Thread thread = new Thread() {
                public void run() {
                    latch.countDown();
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Thread.currentThread().getName() = " + Thread.currentThread().getName());
                    trancaPropagationA.update14(userNamePwdParamUpdate);
                }
            };
            thread.start();
        }

    }


    /**
     * 废弃
     * <p>
     * 循环更新
     * 每次更新方法睡2秒钟
     * 主方法是NOT_SUPPORTED
     * 第一个更新的是在本类中
     */
    @Test
    public void fun23() throws InterruptedException {

        trancaPropagationA.update13(userNamePwdParamUpdate);
    }


    @Test
    public void fun22() {

        trancaPropagationA.update12(userNamePwdParamUpdate);
    }


    /**
     * 废弃
     * <p>
     * 首先 数据库里面开启事务，update
     * 然后执行该方法，传播特性为NOT_SUPPORTED
     * 然后删除方法是提取成一个方法，传播特性为REQUIRES_NEW
     * 期望： 这边正常执行
     * 与期望不符，这边无法正常执行，报死锁。
     * <p>
     * <p>
     * * 首先，然执行该方法，传播特性为NOT_SUPPORTED
     * 提取成一个方法，传播特性为REQUIRES_NEW
     * * 然后， 数据库里面开启事务，update
     * * 期望：能更新，如果传播特性为REQUIRES_NEW 则数据库死锁无法更新
     */
    @Test
    public void fun21() {

        trancaPropagationA.update11(userNamePwdParamUpdate);
    }

    /**
     * 首先，然执行该方法，传播特性为NOT_SUPPORTED
     * 然后， 数据库里面开启事务，update
     * 期望：能更新，如果传播特性为REQUIRES_NEW 则数据库死锁无法更新
     */
    @Test
    public void fun20() {

        trancaPropagationA.update10(userNamePwdParamUpdate);
    }

    /**
     * todo 测试死锁的代码
     * REQUIRES_NEW
     * todo 坑1： 两个测试的方法需要放在两个不同的类中
     * todo 坑2： 如果操作同一个表，会报错。死锁。
     * 期望：死锁
     */
    @Test
    public void fun19() {

        trancaPropagationA.update09(userNamePwdParamUpdate);
    }


    /**
     * PROPAGATION_SUPPORTS
     * 如果当前环境有事务，就加入到当前事务；
     * 如果没有事务，就以非事务的方式执行
     * todo 坑1： 两个测试的方法需要放在两个不同的类中
     * todo 坑2： 如果操作同一个表，会报错。死锁。
     * 步骤 A无事务 B是PROPAGATION_SUPPORTS ，A报错 ，B不回滚
     * 期望：数据不删，数据不插进去
     */
    @Test
    public void fun18() {

        trancaPropagationA.update07(userNamePwdParamUpdate);
    }

    /**
     * PROPAGATION_SUPPORTS
     * 如果当前环境有事务，就加入到当前事务；
     * 如果没有事务，就以非事务的方式执行
     * todo 坑1： 两个测试的方法需要放在两个不同的类中
     * todo 坑2： 如果操作同一个表，会报错。死锁。
     * 步骤 A开启事务 B是PROPAGATION_SUPPORTS ，A报错 ，B回滚
     * 期望：数据不删，数据不插进去
     */
    @Test
    public void fun17() {

        trancaPropagationA.update07(userNamePwdParamUpdate);
    }

    /**
     * REQUIRES_NEW
     * 会新开启事务，外层事务不会影响内部事务的提交/回滚。
     * 内部事务的异常，会影响外部事务的回滚。
     * todo 坑1： 两个测试的方法需要放在两个不同的类中
     * todo 坑2： 如果操作同一个表，会报错。死锁。
     * 步骤 A开启事务 B是REQUIRES_NEW ，A正常 ，B报错
     * 期望：数据不删，数据不插进去
     */
    @Test
    public void fun16() {

        trancaPropagationA.update06(userNamePwdParamUpdate);
    }


    /**
     * REQUIRES_NEW
     * 会新开启事务，外层事务不会影响内部事务的提交/回滚。
     * 内部事务的异常，会影响外部事务的回滚。
     * todo 坑1： 两个测试的方法需要放在两个不同的类中
     * todo 坑2： 如果操作同一个表，会报错。死锁。
     * 步骤 A开启事务 B是REQUIRES_NEW ，A报错 ，B正常
     * 期望：数据不删，数据插进去
     */
    @Test
    public void fun15() {

        trancaPropagationA.update05(userNamePwdParamUpdate);
    }


    /**
     * MANDATORY
     * 支持当前事务，如果当前没有事务，就抛出异常。
     * todo 坑1： 两个测试的方法需要放在两个不同的类中
     * todo 坑2： 如果操作同一个表，会报错。死锁。
     * 期望：报错 No existing transaction found for transaction marked with propagation 'mandatory'
     */
    @Test
    public void fun14() {

        trancaPropagationA.update04(userNamePwdParamUpdate);
    }

    /**
     * PROPAGATION_NEVER
     * 以非事务方式执行，如果当前存在事务，则抛出异常。
     * todo 坑1： 两个测试的方法需要放在两个不同的类中
     * todo 坑2： 如果操作同一个表，会报错。死锁。
     * 传播特性
     * A默认，B   。B报错后
     * 期望：直接报错 Existing transaction found for transaction marked with propagation 'never'
     */
    @Test
    public void fun13() {

        trancaPropagationA.update03(userNamePwdParamUpdate);
    }

    /**
     * NOT_SUPPORTED
     * 以非事务方式执行操作，如果当前存在事务，就把当前事务挂起。
     * todo 坑1： 两个测试的方法需要放在两个不同的类中
     * todo 坑2： 如果操作同一个表，会报错。死锁。 所以2021年11月17日17:36:15新创建一个t_user_tmp表
     * 传播特性
     * A默认，B
     * A回滚，数据不删 ，A报错后
     * B不回滚，数据插入
     * 期望：t_user_tmp 新增一条数据
     */
    @Test
    public void fun12() {

        trancaPropagationA.update02(userNamePwdParamUpdate);
    }

    /**
     * NOT_SUPPORTED
     * 以非事务方式执行操作，如果当前存在事务，就把当前事务挂起。
     * todo 坑1： 两个测试的方法需要放在两个不同的类中
     * todo 坑2： 如果操作同一个表，会报错。死锁。
     * 传播特性
     * A默认，B   。B报错后
     * A回滚，数据不删
     * B不回滚，数据插入
     * 期望：t_user_tmp 新增一条数据
     */
    @Test
    public void fun11() {

        trancaPropagationA.update(userNamePwdParamUpdate);
    }

    /**
     * 接口中A、B两个方法，A没有事务标签，B也没有，第一个方法有。上层通过A间接调用B，此时事务生效。
     * 数据不会被删除
     */
    @Test
    public void fun10() {

        transaInMethod.updateANoBNoThisYes(userNamePwdParamUpdate);
    }

    /**
     * 接口中A、B两个方法，A有@Transactional标签，B也有，第一个方法也有。上层通过A间接调用B，此时事务生效。
     * 数据不会被删除
     */
    @Test
    public void fun09() {

        transaInMethod.updateAYesBYesThisYes(userNamePwdParamUpdate);
    }

    /**
     * 接口中A、B两个方法，A有@Transactional标签，B也有，上层通过A间接调用B，此时事务不生效。
     * 数据会被删除
     */
    @Test
    public void fun08() {

        transaInMethod.updateAYesBYes(userNamePwdParamUpdate);
    }

    /**
     * 接口中A、B两个方法，A有@Transactional标签，B没有，上层通过A间接调用B，此时事务不生效。
     * 数据会被删除
     */
    @Test
    public void fun07() {

        transaInMethod.updateAYesBNo(userNamePwdParamUpdate);
    }

    /**
     * 接口中A、B两个方法，A无@Transactional标签，B有，上层通过A间接调用B，此时事务不生效。
     * 数据会被删除
     */
    @Test
    public void fun06() {

        transaInMethod.updateANoBYes(userNamePwdParamUpdate);
    }


    /**
     *
     * 测试新线程中报错是否会影响主线程中事务回滚
     * 修改
     * 开启事务
     * 查询
     * 另起一个线程删除
     * 新增
     * 查询 抛出异常
     * 提交事务
     *
     * 期望：不会滚。数据会删除，新增会被回滚
     */
    @Test
    public void fun05() {

        userServiceUpdate.updateThread(userNamePwdParamUpdate);
    }


    /**
     * 测试报错是否会事务回滚
     * 修改
     * 开启事务
     * 查询
     * 删除
     * 新增
     * 提交事务
     * 期望：是。会新增数据
     */
    @Test
    public void fun04() {

        userServiceUpdate.update(userNamePwdParamUpdate);
    }

    /**
     * 注册
     * 开启事务
     * 查询
     * 新增
     * 提交事务
     */
    @Test
    public void fun03() {

        userNamePwdParam.setUsername("xxxxxxxxx11");
        userServiceUpdate.register(userNamePwdParam);
    }

    /**
     * 测试读是否会加共享锁
     * 登录
     * 期望：不会
     */
    @Test
    public void fun02() {

        userServiceQuery.login(userNamePwdParamLogin);
    }


    /**
     * 不解释 第一句话就是  hello world
     */
    @Test
    public void fun01() {

        System.out.println("hello world");
    }


}
