package xie;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.meta.Application;
import com.meta.app.entity.MemberEntity;
import com.meta.app.mapper.MemberMapper;

/**
 * @Author: xieZW
 * @Date: 2021/11/26 10:00
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
//@Transactional
public class MemberTest {

    @Autowired
    private MemberMapper memberMapper;

    @Test
    public void getInfo() {
        MemberEntity memberEntity = memberMapper.selectByUniId("ocSHz4gsncqbttD_jUjv8FEHdd3A");
        System.out.println("memberEntity.getUnionId() = " + memberEntity.getUnionId());
    }


    @Test
    public void insertInfo() {
        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setUnionId("123");
        int insert = memberMapper.insertInfo(memberEntity);
        assert insert == 1;
    }


}
