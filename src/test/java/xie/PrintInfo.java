package xie;

import lombok.SneakyThrows;

/**
 * @Author: xieZW
 * @Date: 2022/1/20 16:17
 */
public class PrintInfo implements Runnable {


    @SneakyThrows
    @Override
    public void run() {
        Long commen = FalseHashMap.commen;
        String name = Thread.currentThread().getName();
        System.out.println(name + "!!!!!" + commen);

        Thread.sleep(100);
        FalseHashMap.commen = Thread.currentThread().getId();
        commen = FalseHashMap.commen;
        System.out.println(name + "!!!!!" + commen);
        Thread.sleep(100);
    }
}
