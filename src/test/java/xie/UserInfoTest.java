package xie;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.meta.Application;
import com.meta.api.mapper.UserMapper;
import com.meta.api.param.UserParam;
import com.meta.api.po.UserModel;
import com.meta.api.service.UserServiceQuery;
import com.meta.api.service.UserServiceUpdate;
import com.meta.common.vo.PageInfoVO;
import java.util.HashMap;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2021/11/19 12:07
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@Transactional
public class UserInfoTest {

    @Autowired
    private UserServiceQuery userServiceQuery;

    @Autowired
    private UserServiceUpdate userServiceUpdate;

    @Autowired
    private UserMapper userMapper;


    @Test
    public void fun03() {

        HashMap<String, Object> map = new HashMap<>();
        String zz = map.get("zz").toString();
        System.out.println("zz = " + zz);
    }


    /**
     * 分页查询用户
     */
    @Test
    public void fun02() {

        UserParam userParam = new UserParam();
        userParam.setPage(1);
        userParam.setLimit(10);
        userParam.setUsername("");
        PageInfoVO<List<UserModel>> userModels = userServiceQuery.queryUserList(userParam);
        assert userModels != null;
        List<UserModel> data = userModels.getData();
        assert CollectionUtils.isNotEmpty(data);
        for (UserModel userModel : userModels.getData()) {
            System.out.println(userModel.getEmail());
            System.out.println(userModel.getUsername());
        }
    }

    /**
     * 查询所有用户
     */
    @Test
    public void fun01() {

        List<UserModel> userModels = userMapper.selectAll();
        if (CollectionUtils.isNotEmpty(userModels)) {
            for (UserModel userModel : userModels) {
                System.out.println(userModel.getEmail());
            }
        }
    }
}
