package xie;

/**
 * @Author: xieZW
 * @Date: 2023/2/7 18:50
 */
public class GuPiao {

    //初始资金
    static double initMoney = 20500d;

    //每次获利点数 /每周
    static int step = 2;

    //总金额
    static double sumMoney = 0d;

    //一年操作周数
    static int weeks = 40;

    public static void main(String[] args) {

        int year = 1;
        weeks = weeks * year;
        sumMoney = initMoney;
        for (int i = 0; i < weeks; i++) {
            double moreMoney = sumMoney * 2 / 100;
            System.out.println("第" + (i + 1) + "周获利款项应为：" + moreMoney + "元---------");
            sumMoney = moreMoney + sumMoney;
            System.out.println("第" + (i + 1) + "周账户余额应为：" + sumMoney + "元");
        }
        System.out.println("经过" + year + "年后总余额应为：" + sumMoney + "元");

    }
}
