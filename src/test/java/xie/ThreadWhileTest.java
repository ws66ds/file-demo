package xie;

import lombok.SneakyThrows;

/**
 * @Author: xieZW
 * @Date: 2022/2/7 17:41
 */
public class ThreadWhileTest {

    public static void main(String[] args) {
        Thread a = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {

                while (true) {
                    System.out.println("printing ...");
                    Thread.sleep(5000);
                }

            }
        });
        a.start();
        Runtime.getRuntime().addShutdownHook(a);

    }

}
