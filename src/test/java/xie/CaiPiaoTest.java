package xie;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.meta.Application;
import com.meta.api.mapper.UserMapper;
import com.meta.api.po.UserModel;
import com.meta.common.enums.CommonConstants;
import com.meta.common.utils.DoubleColorUtils;
import com.meta.common.utils.RegexUtils;
import com.meta.common.utils.SendEmail;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @Author: xieZW
 * @Date: 2021/11/19 16:49
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@Transactional
public class CaiPiaoTest {

    static List<String> emailList = new ArrayList();

    static {
        emailList.add("597925798@qq.com");
    }

    static String[] redChar = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
            "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
            "31", "32", "33"};
    static String[] blueChar = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
            "11", "12", "13", "14", "15", "16"};
    @Autowired
    private RedisTemplate redisTemplate;


    public static void main(String[] args) {
        //获取所有的中奖号码
        List<String> list = DoubleColorUtils.historyList("");
        String num = DoubleColorUtils.analyzeInfo(list);
        System.out.println("num = " + num);
    }

    @Test
    public void sendToEmail() {

        System.out.println("xczxczxc哈哈哈" + Charset.defaultCharset());

        getUserInfo();

        //从数据库读取接收人账号数据

        //只发10个
        int num = 0;
        for (String email : emailList) {

            if (num > 10) {
                return;
            }
            //生成彩票数字
            String colorNum = getColorNum();

            String info = "最高出现次数号码为：" + "<br>" + redisTemplate.opsForValue().get(CommonConstants.double_color_ball_history_high_num_key) + "<br>" + "您的幸运号码为" + "<br>" + colorNum;
//            String info = "最高出现次数号码为：" + "02 03 04 05 06 07 08" + "<br>" + colorNum;

            //发送邮件  todo
            SendEmail.sendQQEmail(info, email, "您的双色球一千万大奖号码");
            num++;
        }


        System.out.println("结束！");

    }


    @Test
    public void getFromRedis() {

        String info = "最高出现次数号码为：" + redisTemplate.opsForValue().get(CommonConstants.double_color_ball_history_high_num_key) + "<br> xxx";
        System.out.println("info = " + info);
    }

    @Test
    public void setToRedis() {

        //获取所有的中奖号码
        List<String> list = DoubleColorUtils.historyList("");
        String num = DoubleColorUtils.analyzeInfo(list);

        System.out.println("num = " + num);
        redisTemplate.opsForValue().set(CommonConstants.double_color_ball_history_high_num_key, num);
    }


    @Autowired
    private UserMapper userMapper;

    private void getUserInfo() {
        //查询用户邮箱数据
        List<UserModel> userModels = userMapper.selectAll();
        if (CollectionUtils.isNotEmpty(userModels)) {
            for (UserModel userModel : userModels) {
                String email = userModel.getEmail();
                if (!StringUtils.isEmpty(email) && !RegexUtils.checkEmail(email)) {
                    if (!emailList.contains(email)) {
                        emailList.add(userModel.getEmail());
                    }
                }
            }
        }
    }

    /**
     * 生成彩票数字
     */
    private static String getColorNum() {

        List<String> redList = new ArrayList<>();
        List<String> blueList = new ArrayList<>();
        for (String c : redChar) {
            redList.add(c);
        }
        for (String c : blueChar) {
            blueList.add(c);
        }

        String TenMillionBonusNum = "";
        List<Integer> nums = new ArrayList<>();
        Random r = new Random();

        for (int i = 32; i > 26; i--) {
            System.out.print("现在抽取您的第" + (33 - i) + "个吉祥数字     ");
            int index = r.nextInt(i);
            String num = redList.get(index);
            System.out.println("第" + (33 - i) + "个吉祥数字: " + num);
            redList.remove(index);
            nums.add(Integer.valueOf(num));
        }

        Object[] a = nums.toArray();
        Arrays.sort(a);
        System.out.println("nums = " + nums);
        String numStr = "";
        for (int i = 0; i < a.length; i++) {
            Integer num = (Integer) a[i];
            if (num < 10) {
                numStr = "0" + num;
            } else {
                numStr = num + "";
            }
            if (i == 0) {
                TenMillionBonusNum = numStr + "  ";
            } else {
                TenMillionBonusNum = TenMillionBonusNum + numStr + "  ";
            }
        }
        System.out.print("现在抽取您的第" + 7 + "个吉祥数字     ");
        int index = r.nextInt(16);
        String num = blueList.get(index);
        System.out.println("第" + 7 + "个吉祥数字: " + num);
        nums.add(Integer.valueOf(num));
        TenMillionBonusNum = TenMillionBonusNum + num + "  ";
        System.out.println("恭喜主人喜中1000万RMB，您的兑奖号码为：" + TenMillionBonusNum);
        return TenMillionBonusNum;
    }
}
