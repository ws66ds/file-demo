package leecode.NodeListTest;

/**
 * @Author: xieZW
 * @Date: 2022/2/8 13:48
 */
public class JListNodeChange {
    /**
     * 定义链表
     */
    static class ListNode {                //类名：Java类就是一种自定义的数据结构。
        int val;                          //数据：节点数据。
        JListNodeInsert.ListNode next;                    //对象：引用下一个节点对象。在Java中没有指针的概念，Java中的引用和C语言的指针类似。

        ListNode(int val) {                //构造方法：构造方法和类名相同
            this.val = val;                 //把接收的参数赋值给当前类的val变量
        }
    }

    /**
     * 创建链表及在链表特定位置插入新的节点
     */
    public static void main(String[] args) {

        JListNodeInsert.ListNode head = new JListNodeInsert.ListNode(0);   //创建头节点
        JListNodeInsert.ListNode nextNode;               //声明一个变量用来在移动过程中指向当前节点
        nextNode = head;                   //指向头节点

        //创建链表
        for (int i = 1; i < 10; i++) {
            JListNodeInsert.ListNode node = new JListNodeInsert.ListNode(i);//生成新的节点
            nextNode.next = node;           //把节点连起来
            nextNode = nextNode.next;       //把当前节点往后移动
        }//当for循环完成之后，nextNode指向最后一个节点

        nextNode = head;//重现赋值让它指向头节点
        print(nextNode);//打印输出

        while (nextNode != null) {
            if (nextNode.val == 5) {
                JListNodeInsert.ListNode newnode = new JListNodeInsert.ListNode(99);//生成新的节点
                JListNodeInsert.ListNode node = nextNode.next.next;//先保存下一个节点
                nextNode.next.next = null; //被替换节点指向为空，等待垃圾回收
                nextNode.next = newnode;//插入新节点
                newnode.next = node;//新节点的下一个节点指向之前保存的节点
            }
            nextNode = nextNode.next;
        }//循环完成之后，nextNode指向最后一个节点
        nextNode = head;//重新赋值让它指向头节点
        print(nextNode);//打印输出
    }

    static void print(JListNodeInsert.ListNode listNode) {
        //创建链表节点
        while (listNode != null) {
            System.out.println("节点：" + listNode.val);
            listNode = listNode.next;
        }
        System.out.println();
    }
}
