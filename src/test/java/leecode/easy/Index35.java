package leecode.easy;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/1/22 11:16
 * <p>
 * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
 * <p>
 * 请必须使用时间复杂度为 O(log n) 的算法。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/search-insert-position
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class Index35 {

    @Test
    public void permute() {
        int[] nums = {1, 2, 3};
        List<List<Integer>> lists = new ArrayList<>();
        List<Integer> temp = new ArrayList<>();
        List<Integer> list = null;
        for (int num : nums) {
            temp.add(num);
        }
        //all answers is nums.length!
        int length = nums.length;
        HashSet<Object> set = new HashSet<>();
        //move a num to any index exclude itself
        for (int i = 0; i < length; i++) {
            //now is zhe index num 0 ,let move from 0 to end
            for (int i1 = 0; i1 < length; i1++) {
//                if (i1 == i) {
//                    continue;
//                }
                list = new ArrayList<>(temp);
                int tempNum = list.get(i1).intValue();
                int tempNum2 = list.get(i).intValue();
                list.set(i, tempNum);
                list.set(i1, tempNum2);
                String o = list.toString();
                if (set.contains(o)) {
                    continue;
                }
                set.add(o);
                lists.add(list);
            }
        }
        System.out.println("set.toString() = " + set.toString());
        System.out.println("lists.toString() = " + lists.toString());
    }


}
