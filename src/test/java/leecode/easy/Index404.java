package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 17:26
 * 给定二叉树的根节点 root ，返回所有左叶子之和
 */
public class Index404 {

    public int sumOfLeftLeaves(TreeNode root) {

        if (root == null) {
            return 0;
        }
        return sumOfLeftLeaves(root.left)
                + sumOfLeftLeaves(root.right)
                + (root.left != null && root.left.left == null && root.left.right == null ? root.left.val : 0);
    }
}
