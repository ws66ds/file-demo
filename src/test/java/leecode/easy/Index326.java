package leecode.easy;

import org.junit.Test;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 15:21
 */
public class Index326 {

    @Test
    public void fun() {
        boolean powerOfThree = isPowerOfThree(27);
        System.out.println("powerOfThree = " + powerOfThree);
    }

    public boolean isPowerOfThree(int n) {


        int sum = 3;
        while (sum > 0 && sum < n) {
            sum = sum * 3;
        }
        return n == 1 || sum == n;
    }
}
