package leecode.easy;

import java.util.HashSet;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 11:27
 * 存在重复元素
 * 给你一个整数数组 nums 。如果任一值在数组中出现 至少两次 ，返回 true ；如果数组中每个元素互不相同，返回 false 。
 * 输入：nums = [1,2,3,1]
 * 输出：true
 * 输入：nums = [1,2,3,4]
 * 输出：false
 */
public class Index217 {

    public boolean containsDuplicate(int[] nums) {

        HashSet<Object> set = new HashSet<>();
        for (int num : nums) {
            if (set.contains(num)) {
                return true;
            }
            set.add(num);
        }
        return false;
    }
}
