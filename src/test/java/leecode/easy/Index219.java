package leecode.easy;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 11:43
 * 给你一个整数数组 nums 和一个整数 k ，
 * 判断数组中是否存在两个 不同的索引 i 和 j ，满足 nums[i] == nums[j] 且 abs(i - j) <= k 。如果存在，返回 true ；否则，返回 false 。
 */
public class Index219 {

    @Test
    public void fun() {
        int[] a = {99, 99};
        boolean b = containsNearbyDuplicate(a, 2);
        System.out.println("b = " + b);
    }

    public boolean containsNearbyDuplicate(int[] nums, int k) {

        Set<Object> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            int a = nums[i];
            if (!set.add(a)) {
                if (i - k >= 0) {
                    for (int j = i - k; j < i; j++) {
                        int b = nums[j];
                        if (a == b) {
                            return true;
                        }
                    }
                } else {
                    for (int j = 0; j < i; j++) {
                        int b = nums[j];
                        if (a == b) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
