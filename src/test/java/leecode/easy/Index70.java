package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/3/29 12:04
 * 爬楼梯
 * 找规律
 */
public class Index70 {

    public int climbStairs(int n) {

        int x = 0, y = 0, temp = 1;
        for (int i = 0; i < n; i++) {
            x = y;
            y = temp;
            temp = x + y;
        }
        return temp;
    }
}
