package leecode.easy;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/3/29 16:04
 * 杨辉三角
 */
public class Index118 {

    public List<List<Integer>> generate(int numRows) {

        List<List<Integer>> list = new ArrayList<>();

        List<Integer> innerList = null;
        for (int i = 1; i <= numRows; i++) {
            innerList = new ArrayList<>();
            for (int j = 1; j <= i; j++) {
                if (j == 0 || j == i) {
                    innerList.add(1);
                } else {
                    innerList.add(list.get(i - 1).get(j - 1) + list.get(i - 1).get(j));
                }
                list.add(innerList);
            }
        }
        return list;

        //List<List<Integer>> ret = new ArrayList<List<Integer>>();
        //        for (int i = 0; i < numRows; ++i) {
        //            List<Integer> row = new ArrayList<Integer>();
        //            for (int j = 0; j <= i; ++j) {
        //                if (j == 0 || j == i) {
        //                    row.add(1);
        //                } else {
        //                    row.add(ret.get(i - 1).get(j - 1) + ret.get(i - 1).get(j));
        //                }
        //            }
        //            ret.add(row);
        //        }
        //        return ret;
        //
        //作者：LeetCode-Solution
        //链接：https://leetcode-cn.com/problems/pascals-triangle/solution/yang-hui-san-jiao-by-leetcode-solution-lew9/
        //来源：力扣（LeetCode）
        //著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
    }
}
