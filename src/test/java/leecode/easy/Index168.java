package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/11 12:28
 * 给你一个整数 columnNumber ，返回它在 Excel 表中相对应的列名称
 * 输入：columnNumber = 1
 * 输出："A"
 * <p>
 * 输入：columnNumber = 2147483647
 * 输出："FXSHRXW"
 */
public class Index168 {

    //进制转换  26进制
    public String convertToTitle(int columnNumber) {

        StringBuffer sb = new StringBuffer();
        while (columnNumber > 0) {
            int a0 = (columnNumber - 1) % 26 + 1;
            //todo 转化为字母这里牛逼
            sb.append((char) (a0 - 1 + 'A'));
            columnNumber = (columnNumber - a0) / 26;
        }
        return sb.reverse().toString();
    }

}
