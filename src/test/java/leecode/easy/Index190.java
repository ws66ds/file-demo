package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/24 16:28
 * 颠倒二进制位
 * 颠倒给定的 32 位无符号整数的二进制位
 * 输入：n = 00000010100101000001111010011100
 * 输出：964176192 (00111001011110000010100101000000)
 * 解释：输入的二进制串 00000010100101000001111010011100 表示无符号整数 43261596，
 * 因此返回 964176192，其二进制表示形式为 00111001011110000010100101000000。
 */
public class Index190 {

    public int reverseBits(int n) {

//        String str = Integer.toBinaryString(n);
//        char[] chars = str.toCharArray();
//        StringBuffer sb = new StringBuffer();
//        for (char aChar : chars) {
//            sb.append(aChar);
//        }
//        String finalStr = sb.toString();
//        //转为整数
//        Integer.parseUnsignedInt()
        return Integer.reverse(n);
    }
}
