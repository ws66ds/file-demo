package leecode.easy.tree;

/**
 * @Author: xieZW
 * @Date: 2022/11/29 16:43
 * <p>
 * 左右这俩树 是否一致
 */
public class Index101 {

//    private TreeNode ;

    public boolean isSymmetric(TreeNode root) {

        TreeNode l = root;
        TreeNode r = root;

        return isSymmetricm(l, r);
    }

    private boolean isSymmetricm(TreeNode l, TreeNode r) {

        //     p == null && q == null ? true : (p == null || q == null) ? false : p.val != q.val ? false : isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
        return l == null && r == null ? true : (l == null || r == null) ? false : (l.val != r.val) ? false : isSymmetricm(l.left, r.right) && isSymmetricm(l.right, r.left);
    }

}
