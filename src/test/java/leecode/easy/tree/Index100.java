package leecode.easy.tree;

/**
 * @Author: xieZW
 * @Date: 2022/11/29 15:51
 * 随便一个序遍历，比较的当前节点值一样，继续直到结束，不一样则返回不一致
 */
public class Index100 {

    public boolean isSameTree(TreeNode p, TreeNode q) {

        //都是空
        //一个是空
        //两个都不是空时比较val 然后递归比较
        return p == null && q == null ? true : (p == null || q == null) ? false : p.val != q.val ? false : isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

}
