package leecode.easy.tree;

/**
 * @Author: xieZW
 * @Date: 2022/12/1 15:05
 */
public class Index112 {

    public boolean hasPathSum(TreeNode root, int targetSum) {
        return left(root, targetSum) == 0 || right(root, targetSum) == 0;
    }

    private int left(TreeNode root, int targetSum) {
        if (root == null) {
            targetSum = targetSum - 0;
            return targetSum;
        } else {
            targetSum = targetSum - root.val;
        }

        return left(root.left, targetSum);
    }

    private int right(TreeNode root, int targetSum) {
        if (root == null) {
            targetSum = targetSum - 0;
            return targetSum;
        } else {
            targetSum = targetSum - root.val;
        }
        return right(root.right, targetSum);
    }
}
