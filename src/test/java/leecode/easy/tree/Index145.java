package leecode.easy.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/12/1 15:28
 */
public class Index145 {

    public List<Integer> postorderTraversal(TreeNode root) {

        List<Integer> list = new ArrayList<>();
        getVal(root, list);
        return list;
    }

    private void getVal(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        getVal(root.left, list);
        getVal(root.right, list);
        list.add(root.val);
    }
}
