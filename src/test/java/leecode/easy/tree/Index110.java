package leecode.easy.tree;

/**
 * @Author: xieZW
 * @Date: 2022/11/30 16:58
 */
public class Index110 {

    public boolean isBalanced(TreeNode root) {

        if (root == null) {
            return true;
        }
        deep(root);
        return Math.abs((deep(root.left) - deep(root.right))) <= 1 && isBalanced(root.left) && isBalanced(root.right);
    }

    private int deep(TreeNode root) {

        if (root == null) {
            return 0;
        }
        return Math.max(deep(root.left), deep(root.right)) + 1;
    }
}
