package leecode.easy.tree;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/12/1 16:05
 */
public class Index404 {

    @Test
    public void main() {
        TreeNode treeNode = new TreeNode();
        treeNode.val = 3;
        treeNode.left = new TreeNode();
        treeNode.left.val = 9;
        treeNode.right = new TreeNode();
        treeNode.right.val = 20;
        treeNode.right.left = new TreeNode();
        treeNode.right.left.val = 15;
        treeNode.right.right = new TreeNode();
        treeNode.right.right.val = 7;

        int i = sumOfLeftLeaves(treeNode);
        System.out.println("i = " + i);
    }

    public int sumOfLeftLeaves(TreeNode root) {

        int sum = 0;
        List<Integer> list = new ArrayList<>();
        sdas(root, list);
        for (Integer integer : list) {
            sum = sum + integer;
        }
        return sum;
    }

    private void sdas(TreeNode root, List sum) {
        if (root == null) {
            return;
        }
        if (root.left != null && root.left.left == null) {
            sum.add(root.left.val);
        }
        sdas(root.left, sum);
        sdas(root.right, sum);
    }


}
