package leecode.easy.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/12/1 15:52
 */
public class Index257 {

    public List<String> binaryTreePaths(TreeNode root) {


        List<String> list = new ArrayList<>();
        adad(root, list);
        return list;
    }

    private void adad(TreeNode root, List<String> list) {

        if (root == null) {
            return;
        }
        list.add(String.valueOf(root.val));
        adad(root.left, list);
        adad(root.right, list);
    }
}
