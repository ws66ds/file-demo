package leecode.easy.tree;

/**
 * @Author: xieZW
 * @Date: 2022/11/30 17:05
 */
public class Index111 {

    public int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.min(minDepth(root.left), minDepth(root.right)) + 1;
    }
}
