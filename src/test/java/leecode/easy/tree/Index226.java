package leecode.easy.tree;

/**
 * @Author: xieZW
 * @Date: 2022/12/1 15:32
 */
public class Index226 {

    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        TreeNode leftNode = invertTree(root.left);
        TreeNode rightNode = invertTree(root.right);
        root.left = rightNode;
        root.right = leftNode;
        return root;
    }
}
