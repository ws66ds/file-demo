package leecode.easy.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/12/1 15:25
 */
public class Index144 {
    public List<Integer> preorderTraversal(TreeNode root) {

        List<Integer> list = new ArrayList<>();
        dasdad(root, list);
        return list;
    }

    private void dasdad(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        list.add(root.val);
        dasdad(root.left, list);
        dasdad(root.right, list);
    }
}
