package leecode.easy;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/2/10 10:11
 * 给定一个非负索引 rowIndex，返回「杨辉三角」的第 rowIndex 行。
 * <p>
 * 在「杨辉三角」中，每个数是它左上方和右上方的数的和。
 */
public class Index119 {

    @Test
    public void fun() {
        getRow(3);
    }

    public List<Integer> getRow(int rowIndex) {

        List<List<Integer>> list = new ArrayList<>();
        for (int i = 0; i <= rowIndex; i++) {
            List<Integer> list1 = new ArrayList<>();
            for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i) {
                    list1.add(1);
                } else {
                    list1.add(list.get(i - 1).get(j - 1) + list.get(i - 1).get(j));
                }
            }
            list.add(list1);
        }
        return list.get(rowIndex);
    }
}
