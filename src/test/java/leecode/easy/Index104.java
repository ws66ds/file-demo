package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/8 18:15
 */
public class Index104 {

    public int maxDepth(TreeNode root) {

        Integer deep = 0;
        if (root == null) {
            return deep;
        }
        Integer dfs = maxDepth(root.left);
        Integer dfs1 = maxDepth(root.right);
        return dfs > dfs1 ? dfs + 1 : dfs1 + 1;
    }

}
