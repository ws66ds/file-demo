package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/8 17:15
 */
public class Index100 {


    public boolean isSameTree(TreeNode p, TreeNode q) {
        return dfs(p, q);
    }

    boolean dfs(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        } else if (p == null && q != null) {
            return false;
        } else if (p != null && q == null) {
            return false;
        } else {
            if (p.val != q.val) {
                return false;
            } else {
                return dfs(p.left, q.left) && dfs(p.right, q.right);
            }
        }
    }
}
