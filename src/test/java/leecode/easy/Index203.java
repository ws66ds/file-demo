package leecode.easy;

import org.junit.Test;

/**
 * @Author: xieZW
 * @Date: 2022/2/24 17:20
 * 移除链表元素
 * 给你一个链表的头节点 head 和一个整数 val ，请你删除链表中所有满足 Node.val == val 的节点，并返回 新的头节点
 * 输入：head = [1,2,6,3,4,5,6], val = 6
 * 输出：[1,2,3,4,5]
 * 输入：head = [], val = 1
 * 输出：[]
 */
public class Index203 {

    @Test
    public void fun() {
        ListNode listNode = new ListNode(1);
        listNode.next = new ListNode(2);
        listNode.next.next = new ListNode(6);
        listNode.next.next.next = new ListNode(3);
        listNode.next.next.next.next = new ListNode(4);
        listNode.next.next.next.next.next = new ListNode(5);
        listNode.next.next.next.next.next.next = new ListNode(6);
        ListNode listNode1 = removeElements(listNode, 6);
        System.out.println("listNode1 = " + listNode1);
    }

    public ListNode removeElements(ListNode head, int val) {

        if (head == null) {
            //find the last node end
            return head;
        }
        //从头到尾遍历一遍
        head.next = removeElements(head.next, val);
        //根据条件 换指向
        return head.val == val ? head.next : head;
    }

}
