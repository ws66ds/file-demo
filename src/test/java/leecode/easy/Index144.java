package leecode.easy;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/2/10 14:20
 * 给你二叉树的根节点 root ，返回它节点值的 前序 遍历
 * 输入：root = [1,null,2,3]
 * 输出：[1,2,3]
 */
public class Index144 {

    public List<Integer> preorderTraversal(TreeNode root) {

        List<Integer> list = new ArrayList<>();
        dfs(root, list);
        return list;
    }

    private void dfs(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        list.add(root.val);
        dfs(root.left, list);
        dfs(root.right, list);
    }
}
