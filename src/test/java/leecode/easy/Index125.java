package leecode.easy;

import org.junit.Test;

/**
 * @Author: xieZW
 * @Date: 2022/2/10 11:08
 * 给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。
 * <p>
 * 说明：本题中，我们将空字符串定义为有效的回文串。
 * <p>
 * 输入: "A man, a plan, a canal: Panama"
 * 输出: true
 * 解释："amanaplanacanalpanama" 是回文串
 */
public class Index125 {

    @Test
    public void fun() {
        boolean palindrome = isPalindrome("A man, a plan, a canal: Panama");
        System.out.println("palindrome = " + palindrome);
    }

    public boolean isPalindrome(String s) {
        if (s.length() == 0 || s == null) {
            return true;
        }
        char[] chars = s.toCharArray();
        StringBuffer sb = new StringBuffer();
        //只保留字母和数字
        for (char aChar : chars) {
            //todo  神来之笔
            if (Character.isLetterOrDigit(aChar)) {
                sb.append(aChar);
            }
        }
        String s1 = sb.toString();
        //双指针,头尾依次比较
        char[] chars1 = s1.toCharArray();
        for (int i = 0; i < chars1.length; i++) {
            if (i >= chars1.length - 1 - i) {
                return true;
            }
            char c = Character.toLowerCase(chars1[i]);
            char c1 = Character.toLowerCase(chars1[chars1.length - 1 - i]);
            if (c != c1) {
                return false;
            }
        }
        return true;
    }
}
