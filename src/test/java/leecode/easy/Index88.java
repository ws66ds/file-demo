package leecode.easy;

import org.junit.Test;

/**
 * @Author: xieZW
 * @Date: 2022/2/8 14:13
 */
public class Index88 {

    @Test
    public void fun() {
        int[] nums1 = new int[]{-1, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0};
        int[] nums2 = new int[]{-1, -1, 0, 0, 1, 2};
        merge(nums1, 5, nums2, 0);
//        int[] nums1 = new int[]{1, 2, 3, 0, 0, 0};
//        int[] nums2 = new int[]{2, 5, 6};
//        merge(nums1, 3, nums2, 3);
//        int[] nums1 = new int[]{2, 0};
//        int[] nums2 = new int[]{1};
//        merge(nums1, 1, nums2, 1);
//        int[] nums1 = new int[]{0, 0, 0, 0, 0};
//        int[] nums2 = new int[]{1, 2, 3, 4, 5};
//        merge(nums1, 0, nums2, 5);
//        int[] nums1 = new int[]{-12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//        int[] nums2 = new int[]{-49, -45, -42, -41, -40, -39, -39, -39, -38, -36, -34, -34, -33, -33, -32, -31, -29, -28, -26, -26, -24, -21, -20, -20, -18, -16, -16, -14, -11, -7, -6, -5, -4, -4, -3, -3, -2, -2, -1, 0, 0, 0, 2, 2, 6, 7, 7, 8, 10, 10, 13, 13, 15, 15, 16, 17, 17, 19, 19, 20, 20, 20, 21, 21, 22, 22, 24, 24, 25, 26, 27, 29, 30, 30, 30, 35, 36, 36, 36, 37, 39, 40, 41, 42, 45, 46, 46, 46, 47, 48};
//        merge(nums1, 1, nums2, 90);
//        int[] nums1 = new int[]{1, 0};
//        int[] nums2 = new int[]{2};
//        merge(nums1, 1, nums2, 1);
    }

    public void merge(int[] nums1, int m, int[] nums2, int n) {

        if (m == 0 && n != 0) {
            for (int i = 0; i < n; i++) {
                nums1[i] = nums2[i];
            }
            for (int i : nums1) {
                System.out.print("nums1 = " + i + "      ");
            }
            return;
        }
        if (n == 0) {
            nums1 = nums1;
            for (int i : nums1) {
                System.out.print("nums1 = " + i + "      ");
            }
            return;
        }

        int index = 0; // 记录上次插入的位置
        for (int i = 0; i < n; i++) {
            int value2 = nums2[i];
            for (int i1 = index; i1 < m + n; i1++) {
                int value1 = nums1[i1];
                if (value2 < value1) {
                    //需要插入前面，后面的全部后移一位，空出来一个位置
                    index = i1;
                    //所有的元素往后移，然后插入该位置
                    int lastIndex = m + n - 1;
                    while (lastIndex >= i1) {
                        if (lastIndex - 1 >= 0) {
                            nums1[lastIndex] = nums1[lastIndex - 1];
                        }
                        lastIndex--;
                    }
                    //空出位置了，该值插入
                    nums1[i1] = value2;
                    break;
                } else {
                    //需要插入后面，前面的不变，如果是0直接替换，如果不是0，继续跟后面比较。
                    if (value1 == 0) {
                        nums1[i1] = value2;
//                        index = index + 1;
                        index = i1;
                        index++;
//                        i1++;
                        break;
                    }
                    continue;
//                    if (i1 >= m && i1 > index) {
//                        if (nums1[i1] < nums1[i1 - 1]) {
//                            //直接替换0的操作
//                            nums1[i1] = value2;
//                            index = i1;
//                            break;
//                        } else if (nums1[i1] > nums1[i1 - 1] && nums1[i1 - 1] < 0) {
//                            nums1[i1 + 1] = value2;
//                            index = i1;
//                            break;
//                        } else if (nums1[i1] == nums1[i1 - 1] && nums1[i1 - 1] == 0) {
//                            nums1[i1] = value2;
//                            index = i1;
//                            break;
//                        }
//                    }
                }
            }
        }
        for (int i : nums1) {
            System.out.print(i + ",");
        }
    }
}
