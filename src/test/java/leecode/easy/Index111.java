package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/3/29 15:37
 * 最小深度
 */
public class Index111 {

    public int minDepth(TreeNode root) {

        if (root == null) {
            return 0;
        }

        int a = minDepth(root.left);
        int b = minDepth(root.right);
        return a == 0 || b == 0 ? a + b + 1 : Math.min(a, b) + 1;
    }
}
