package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 16:25
 * 输入：s = ["h","e","l","l","o"]
 * 输出：["o","l","l","e","h"]
 */
public class Index344 {

    public void reverseString(char[] s) {
        int start = 0;
        int end = s.length - 1;
        char temp = 0;
        while (start < end) {
            temp = s[start];
            s[start] = s[end];
            s[end] = temp;
            start++;
            end--;
        }
    }
}
