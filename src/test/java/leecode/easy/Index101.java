package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/8 17:55
 */
public class Index101 {

    public boolean isSymmetric(TreeNode root) {
        //一个左边 一个右边，是否为空 且值是否相等
        return dfs(root.left, root.right);
    }

    private boolean dfs(TreeNode left, TreeNode right) {

        if (left == null && right == null) {
            return true;
        } else if (left == null || right == null) {
            return false;
        } else {
            return left.val == right.val && dfs(left.left, right.right) && dfs(left.right, right.left);
        }
    }
}
