package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/9 10:55
 * 给定一个二叉树，判断它是否是高度平衡的二叉树。
 * <p>
 * 本题中，一棵高度平衡二叉树定义为：
 * <p>
 * 一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过 1 。
 */
public class Index110 {

    private static boolean result = true;
    static TreeNode node = null;

    public boolean isBalanced(TreeNode root) {
        //直接插到最后一层为null的,任意一个为null的,如果另一边还有超过一层,返回false
        if (root == null) {
            return result;
        }
        //层序遍历 某一行有null 且还有下一行 那么不是高度为1的
        dfs(root);

        return result;
    }

    private void dfs(TreeNode root) {
        if (!result) {
            return;
        }
        if (root.left == null) {
            //左边为空,就判断右边是否有值,如果右边有值,那么就是假的
            if ((node.left != null && node.left.val == root.left.val)) {
                if (node.left.left != null || node.left.right != null) {
                    result = false;
                    return;
                }
            }
            if ((node.right != null && node.right.val == root.left.val)) {
                if (node.right.left != null || node.right.right != null) {
                    result = false;
                    return;
                }
            }
        }
        if (root.right == null) {
            //左边为空,就判断右边是否有值,如果右边有值,那么就是假的
            if ((node.left != null && node.left.val == root.left.val)) {
                if (node.left.left != null || node.left.right != null) {
                    result = false;
                    return;
                }
            }
            if ((node.right != null && node.right.val == root.left.val)) {
                if (node.right.left != null || node.right.right != null) {
                    result = false;
                    return;
                }
            }
        }
        node = root;
        dfs(root.left);
        dfs(root.right);
    }
}
