package leecode.easy;

import org.junit.Test;

/**
 * @Author: xieZW
 * @Date: 2022/2/10 11:28
 * 给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
 * <p>
 * 说明：
 * <p>
 * 你的算法应该具有线性时间复杂度。 你可以不使用额外空间来实现吗？
 * 输入: [2,2,1]
 * 输出: 1
 */
public class Index136 {

    @Test
    public void fun() {
//        int[] a = {2, 2, 1};
        int[] a = {4, 1, 2, 1, 2};
        int i = singleNumber(a);
        System.out.println(i);
    }

    public int singleNumber(int[] nums) {

        int single = 0;
        for (int num : nums) {
            single = single ^ num;
        }
        return single;

//        Arrays.sort(nums);
//        for (int i = 0; i < nums.length; i++) {
//            if (i + 1 < nums.length && nums[i] == nums[i + 1]) {
//                i++;
//            } else {
//                return nums[i];
//            }
//        }
//        return 0;
    }
}
