package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/24 16:48
 */
public class Index191 {

    // you need to treat n as an unsigned value
    public int hammingWeight(int n) {

        return Integer.bitCount(n);
    }
}
