package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 11:13
 * 输入：s = "egg", t = "add"
 * 输出：true
 */
public class Index205 {

    public boolean isIsomorphic(String s, String t) {


        for (int i = 0; i < s.length(); i++) {
            if (s.indexOf(s.charAt(i)) != t.indexOf(t.charAt(i))) {
                return false;
            }
        }
        return true;

//        Map<Object, String> map = new HashMap<>();
//        char[] chars = s.toCharArray();
//        for (int i = 0; i < chars.length; i++) {
//            String o = map.get(chars[i]);
//            if (o == null) {
//                map.put(chars[i], i + ",");
//            } else {
//                map.put(chars[i], o + i + ",");
//            }
//        }
//
//        Map<Object, String> map2 = new HashMap<>();
//        char[] chars2 = t.toCharArray();
//        for (int i = 0; i < chars2.length; i++) {
//            String o = map2.get(chars2[i]);
//            if (o == null) {
//                map2.put(chars2[i], i + ",");
//            } else {
//                map2.put(chars2[i], o + i + ",");
//            }
//        }
//
//        return false;
    }
}
