package leecode.easy;

import org.junit.Test;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 13:45
 */
public class Index231 {

    @Test
    public void fun() {
        boolean powerOfTwo = isPowerOfTwo(1);
        System.out.println("powerOfTwo = " + powerOfTwo);
    }

    public boolean isPowerOfTwo(int n) {
        if (n == 1) {
            return true;
        }
        while (n > 2) {
            int m = n % 2;
            if (m != 0) {
                return false;
            }
            n = n >> 1;
        }
        return n == 2;
    }
}
