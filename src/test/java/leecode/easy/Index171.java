package leecode.easy;

import leecode.persion;
import org.junit.Test;

import java.util.HashSet;

/**
 * @Author: xieZW
 * @Date: 2022/2/24 15:54
 * Excel 表列序号
 * 给你一个字符串 columnTitle ，表示 Excel 表格中的列名称。返回 该列名称对应的列序号
 * 示例 1:
 * <p>
 * 输入: columnTitle = "A"
 * 输出: 1
 * 示例 2:
 * <p>
 * 输入: columnTitle = "AB"
 * 输出: 28
 * 示例 3:
 * <p>
 * 输入: columnTitle = "ZY"
 * 输出: 701
 */
public class Index171 {


    public static void main(String[] args) {


        persion p = new persion("1");
        persion q = new persion("1");
        System.out.println(p.equals(q));
        HashSet hashSet = new HashSet();
        hashSet.add(p);
        hashSet.add(q);
//        Thread.sleep(1);
//        synchronized ("") {
//            "".wait();
//        }
        System.out.println(hashSet.size());


//        HashMap<Object, Object> map = new HashMap<>();
//        Iterator<Map.Entry<Object, Object>> iterator = map.entrySet().iterator();
//        if (iterator.hasNext()) {
//            iterator.remove();
//        }
//
//
//        String str = "ZY";
//        char[] chars = str.toCharArray();
//        int sum = 0;
//        int length = chars.length;
//        for (int i = chars.length - 1; i >= 0; i--) {
//            int xishu = 1;
//            for (int i1 = 0; i1 < length - 1 - i; i1++) {
//                xishu = xishu * 26;
//            }
//            sum = sum + (chars[i] - 64) * xishu;
//        }
//        System.out.println("sum = " + sum);
    }

    @Test
    public void fun() {
        int a = titleToNumber("FXSHRXW");
        //2147483647
        System.out.println("a = " + a);
    }


    public int titleToNumber(String s) {

        char[] chars = s.toCharArray();
        int sum = 0;
        for (int i = 0; i < chars.length; i++) {
            int a = chars[i] - 64;
            int index = chars.length - 1 - i;
            if (index > 0) {
                sum = (int) (sum + a * (Math.pow(26, index)));
            } else {
                sum = sum + a;
            }
        }
        return sum;
    }
}
