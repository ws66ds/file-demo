package leecode.easy;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author: xieZW
 * @Date: 2022/2/24 15:32
 * 给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数 大于 ⌊ n/2 ⌋ 的元素。
 * <p>
 * 你可以假设数组是非空的，并且给定的数组总是存在多数元素。
 * 输入：[3,2,3]
 * 输出：3
 * 输入：[2,2,1,1,1,2,2]
 * 输出：2
 */
public class Index169 {

    @Test
    public void fun() {
        int[] ints = {2, 2, 1, 1, 1, 2, 2};
        int i = majorityElement(ints);
        System.out.println("i = " + i);
    }

    public int majorityElement(int[] nums) {

        int small = nums.length / 2;
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
//            if (num <= small) {
//                continue;
//            }
            if (null == map.get(num)) {
                map.put(num, 1);
            } else {
                map.put(num, map.get(num) + 1);
            }
        }
        HashMap<Integer, Integer> mapSort = new HashMap<>();
        int max = 0;
        Set<Map.Entry<Integer, Integer>> entries = map.entrySet();
        for (Map.Entry<Integer, Integer> entry : entries) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            mapSort.put(value, key);
            max = Math.max(max, value);
        }
        return mapSort.get(max);
    }
}
