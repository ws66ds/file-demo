package leecode.easy;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 13:54
 * 回文链表
 * 给你一个单链表的头节点 head ，请你判断该链表是否为回文链表。如果是，返回 true ；否则，返回 false 。
 * 输入：head = [1,2,2,1]
 * 输出：true
 */
public class Index234 {

    @Test
    public void fun() {
        ListNode listNode = new ListNode(1);
        listNode.next = new ListNode(2);
        listNode.next.next = new ListNode(2);
        listNode.next.next.next = new ListNode(1);
        boolean palindrome = isPalindrome(listNode);
        System.out.println("palindrome = " + palindrome);
    }

    public boolean isPalindrome(ListNode head) {

        List<Integer> list = new ArrayList<>();
        while (head != null) {
            list.add(head.val);
            head = head.next;
        }
        for (int i = 0; i < list.size(); i++) {
            int a = list.get(i);
            int index = list.size() - 1 - i;
            int b = list.get(index);
            if (i <= index && a != b) {
                return false;
            }
        }
        return true;
    }
}
