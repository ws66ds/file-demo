package leecode.easy;

import org.junit.Test;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 14:40
 */
public class Index263 {

    @Test
    public void fun() {
        boolean ugly = isUgly(-2147483648);
        Long aa = 2147483648L % 7;
        System.out.println("aa = " + aa);
        System.out.println("ugly = " + ugly);
    }

    public boolean isUgly(int n) {
        if (n <= 0) {
            return false;
        }
        if (n % 7 != 0) {
            return false;
        }
        return true;
    }
}
