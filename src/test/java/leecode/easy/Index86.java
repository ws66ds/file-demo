package leecode.easy;

import org.junit.Test;

/**
 * @Author: xieZW
 * @Date: 2022/2/8 10:49
 */
public class Index86 {


    @Test
    public void fun01() {
        ListNode small = new ListNode(0);
        ListNode smallHead = small;

//        while (5-- > 0) {
//            smallHead.val = 1;
//            smallHead.next;
//        }
    }

    @Test
    public void fun() {
        ListNode listNode = new ListNode(1);
        listNode.next = new ListNode(4);
        listNode.next.next = new ListNode(3);
        listNode.next.next.next = new ListNode(2);
        listNode.next.next.next.next = new ListNode(5);
        listNode.next.next.next.next.next = new ListNode(2);
        partition(listNode, 3);
    }

    public ListNode partition(ListNode head, int x) {

        //遍历，使X与每个节点值进行比较，如果val>=x，不用管，继续比较next.val，如果val<x，使该节点等于上一个节点值
        if (head == null) {
            return head;
        }
        ListNode cur = head;
        while (cur.next != null) {//保证节点长度是至少为2的
            if (cur.next.val >= x) {
                if (cur.next.next != null) {
                    cur.next = cur.next.next;
                } else {
                    break;
                }
            } else {
                int temp = cur.val;
                if (temp > cur.next.val) {
                    cur.val = cur.next.val;
                    cur.next.val = temp;
                }
            }
        }
        return head;
    }
}
