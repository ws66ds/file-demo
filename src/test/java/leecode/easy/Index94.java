package leecode.easy;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/2/8 16:45
 */
public class Index94 {

    @Test
    public void fun() {
        TreeNode root = new TreeNode(2);
        TreeNode left01 = new TreeNode(1);
        root.left = left01;
        TreeNode right = new TreeNode(3);
        root.right = right;
        List<Integer> integers = inorderTraversal(root);
        System.out.println("integers = " + integers);
    }


    //中序遍历是 左根右
    public List<Integer> inorderTraversal(TreeNode root) {

        List<Integer> list = new ArrayList<>();
        dfs(root, list);
        return list;
    }

    private void dfs(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        dfs(root.left, list);
        list.add(root.val);
        dfs(root.right, list);
    }
}
