package leecode.easy;

import org.junit.Test;

import java.util.Stack;

/**
 * @Author: xieZW
 * @Date: 2022/2/10 14:24
 * 设计一个支持 push ，pop ，top 操作，并能在
 * todo 常数时间内检索到最小元素的 栈。
 * <p>
 * push(x) —— 将元素 x 推入栈中。
 * pop() —— 删除栈顶的元素。
 * top() —— 获取栈顶元素。
 * getMin() —— 检索栈中的最小元素。
 *  
 * <p>
 * 示例:
 * <p>
 * 输入：
 * ["MinStack","push","push","push","getMin","pop","top","getMin"]
 * [[],[-2],[0],[-3],[],[],[],[]]
 * <p>
 * 输出：
 * [null,null,null,null,-3,null,0,-2]
 * <p>
 * 解释：
 * MinStack minStack = new MinStack();
 * minStack.push(-2);
 * minStack.push(0);
 * minStack.push(-3);
 * minStack.getMin();   --> 返回 -3.
 * minStack.pop();
 * minStack.top();      --> 返回 0.
 * minStack.getMin();   --> 返回 -2.
 *  
 * <p>
 * 提示：
 * <p>
 * pop、top 和 getMin 操作总是在 非空栈 上调用
 */
public class MinStack {

    @Test
    public void fun() {
        MinStack minStack = new MinStack();
        minStack.push(2);
        minStack.push(0);
        minStack.push(3);
        minStack.push(0);
        int min = minStack.getMin();
        System.out.println("min = " + min);
        minStack.pop();
        int min2 = minStack.getMin();
        System.out.println("min2 = " + min2);
        minStack.pop();
        int min3 = minStack.getMin();
        System.out.println("min3 = " + min3);
        minStack.pop();

        int min4 = minStack.getMin();
        System.out.println("min4 = " + min4);
    }

    private static Stack<Integer> stack;
    private static Stack<Integer> smallNum;

    /**
     * initialize your data structure here.
     */
    public MinStack() {

        stack = new Stack<>();
        smallNum = new Stack<>();
        smallNum.push(Integer.MAX_VALUE);
    }

    public void push(int x) {
        stack.push(x);
        smallNum.push(Math.min(smallNum.peek(), x));
    }

    public void pop() {
        stack.pop();
        smallNum.pop();
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {

//        int small = Integer.MAX_VALUE;
//        Iterator<Integer> elements = stack.iterator();
//        while (elements.hasNext()) {
//            int b = elements.next().intValue();
//            small = Math.min(small, b);
//        }
        return smallNum.peek();
    }
}
