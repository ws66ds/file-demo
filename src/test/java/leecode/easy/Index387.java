package leecode.easy;

import org.junit.Test;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 17:20
 */
public class Index387 {

    @Test
    public void fun() {
        int leetcode = firstUniqChar("leetcode");
        System.out.println("leetcode = " + leetcode);
    }

    public int firstUniqChar(String s) {
        //确保唯一才能这么玩
        char[] chars = s.toCharArray();
        char meta = 1;
        for (char aChar : chars) {
            meta = (char) (meta ^ aChar);
        }
        return s.indexOf(meta);
    }
}
