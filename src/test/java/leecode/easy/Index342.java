package leecode.easy;

/**
 * @Author: xieZW
 * @Date: 2022/2/25 15:35
 */
public class Index342 {

    public boolean isPowerOfFour(int n) {

        int sum = 4;
        while (sum > 0 && sum < n) {
            sum = sum * 4;
        }
        return n == 1 || sum == n;
    }
}
