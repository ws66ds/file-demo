package juc;

import org.junit.Test;

/**
 * @Author: xieZW
 * @Date: 2022/3/3 10:56
 */
public class xiancheng {

    @Test
    public void fun12() throws InterruptedException {
        new Thread(new BusiThread()).start();
        System.out.println("Main do ites work........");
        Thread.sleep(10);
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
        System.out.println("Main do ites work........");
    }

    private static class BusiThread implements Runnable {

        public void run() {
            for (int i = 0; i < 3; i++) {
                System.out.println("BusiThread_" + Thread.currentThread().getId()
                        + " do business-----");
            }
        }
    }

    @Test
    public void fun11() {
        ThreadLocal<Object> local = new ThreadLocal<>();
        Thread thread = new Thread();
        local.set(1024 * 1024 * 1024);
    }


    @Test
    public void fun10() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("我优先级最高 我第一个执行");
            }
        });
        thread.setPriority(10);

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("我优先级中等 我第二个执行");
            }
        });
        thread1.setPriority(8);


        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("我优先级最低 我第三个执行");
            }
        });
        thread2.setPriority(6);

        thread2.start();
        thread1.start();
        thread.start();
    }

    @Test
    public void fun() {
        Thread t = new Thread() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        };
//        Thread.yield();
        t.interrupt();
        Thread.interrupted();
        //新建状态
        System.out.println(t.getState().name());
        //可运行状态
        t.start();
        System.out.println(t.getState().name());

    }

    @Test
    public void fun02() {
        final Object lock = new Object();

        Thread t = new Thread() {
            @Override
            public void run() {
                synchronized (lock) {
                    System.out.println(Thread.currentThread().getName() + "...");
                }
            }
        };
        synchronized (lock) {
            try {
                t.start();
                Thread.sleep(1000);
                System.out.println(t.getState().name());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void fun03() {
        final Object lock = new Object();

        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (lock) {
                        lock.wait();
                        System.out.println(Thread.currentThread().getState().name() + "xxxx");
                        System.out.println(Thread.currentThread().getName());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        synchronized (lock) {
            System.out.println(t.getState().name());
            lock.notifyAll();
        }
    }


    @Test
    public void fun04() {
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    System.out.println(Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(t.getState().name());
    }

    @Test
    public void fun07() throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        });
        thread.start();
        Thread.sleep(1000);
        System.out.println(" = ");
        String name = thread.getState().name();
        System.out.println("name = " + name);
    }

    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("111");
            }
        });
        thread.start();
        String name = thread.getState().name();
        System.out.println("222 = " + name);
    }
}
