package juc;

/**
 * @Author: xieZW
 * @Date: 2022/2/17 17:13
 */
public class WaitAndNotify {

    public static void main(String[] args) {
        KafkaSelf kafkaSelf = new KafkaSelf();
        Runnable pro = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 30; i++) {
                    kafkaSelf.pro();
                }
            }
        };

        Runnable cu = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 3; i++) {
                    kafkaSelf.cu();
                }
            }
        };
//        for (int i = 0; i < 1; i++) {
//            new Thread(pro).start();
//        }
        new Thread(pro).start();
        new Thread(cu).start();
//        for (int i = 0; i < 1; i++) {
//            new Thread(cu).start();
//        }

    }
}
