package juc;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @Author: xieZW
 * @Date: 2022/3/2 15:51
 */
public class buer {

    public static void main(String[] args) {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });
        thread.getState();
        thread.start();
        thread.run();


        Boolean aBoolean = new Boolean(true);
        boolean b = aBoolean.booleanValue();
        System.out.println("b = " + b);
        Boolean aTrue = Boolean.TRUE;
        System.out.println("aTrue = " + aTrue);

        AtomicBoolean atomicBoolean = new AtomicBoolean();
        boolean b1 = atomicBoolean.get();
        System.out.println("b1 = " + b1);
    }
}
