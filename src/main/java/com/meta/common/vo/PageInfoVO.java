package com.meta.common.vo;

import lombok.Data;

/**
 * @Author: xieZW
 * @Date: 2021/11/19 23:04
 */
@Data
public class PageInfoVO<T> {

    private T data;

    private Integer count;
}
