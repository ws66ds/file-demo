package com.meta.common.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: xieZW
 * @Date: 2021/11/9 15:11
 */
@Data
public class ResponseInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private int status = 200;
    private String message = "success";
    private Object data;
    private String err = "ok";

    public ResponseInfo(int status, String message, Object data, String err) {
        this.status = status;
        this.message = message;
        this.data = data;
        this.err = err;
    }

    public ResponseInfo() {
    }

    public ResponseInfo(String msg) {
        this.message = msg;
    }

    public ResponseInfo(String msg, Object data) {
        this.message = msg;
        this.data = data;
    }

    public ResponseInfo(int status, String msg) {
        this.status = status;
        this.message = msg;
    }

    public ResponseInfo(int status, String msg, Object data) {
        this.status = status;
        this.message = msg;
        this.data = data;
    }

    public static ResponseInfo error() {
        return error(500, "系统异常，请联系管理员");
    }

    public static ResponseInfo error(String msg) {
        return error(500, msg);
    }

    public static ResponseInfo error(String msg, Object data) {
        return error(500, msg, data);
    }

    public static ResponseInfo error(int code, String msg) {
        return new ResponseInfo(code, msg);
    }

    public static ResponseInfo error(int code, String msg, Object data) {
        return new ResponseInfo(code, msg, data);
    }

    public static ResponseInfo succ(Object data) {
        return (new ResponseInfo()).data(data);
    }

    public static ResponseInfo errorWX(Object data) {
        ResponseInfo data1 = (new ResponseInfo()).data(data);
        data1.setErr("noOk");
        return data1;
    }

    public static ResponseInfo succWX(Object data) {
        ResponseInfo data1 = (new ResponseInfo()).data(data);
        return data1;
    }

    public static ResponseInfo succ(String msg, Object data) {
        return new ResponseInfo(msg, data);
    }

    public static ResponseInfo succ() {
        return new ResponseInfo();
    }

    public ResponseInfo data(Object data) {
        this.data = data;
        return this;
    }

    public int getStatus() {
        return this.status;
    }

    public String getMessage() {
        return this.message;
    }

    public Object getData() {
        return this.data;
    }
}
