package com.meta.common.vo;

import lombok.Data;

/**
 * @Author: xieZW
 * @Date: 2021/12/3 18:05
 */
@Data
public class Token {


    private String token;
}
