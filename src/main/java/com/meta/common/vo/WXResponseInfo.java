package com.meta.common.vo;

import java.io.Serializable;

/**
 * @Author: xieZW
 * @Date: 2021/11/26 12:38
 */
public class WXResponseInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    private int status = 200;
    private String err = "ok";
    private Object data;


    public WXResponseInfo(int status, String err) {
        this.status = status;
        this.err = err;
    }

    public WXResponseInfo(int status, String err, Object data) {
        this.status = status;
        this.err = err;
        this.data = data;
    }

    public WXResponseInfo(Object data) {
        this.data = data;
    }

    public WXResponseInfo() {
    }

    public WXResponseInfo(String err, Object data) {
        this.err = err;
        this.data = data;
    }

    public static WXResponseInfo error(String msg, Object data) {
        return error(500, msg, data);
    }

    public static WXResponseInfo error(int code, String msg, Object data) {
        return new WXResponseInfo(code, msg, data);
    }

    public static WXResponseInfo error(String msg) {
        return error(500, msg);
    }

    public static WXResponseInfo error() {
        return error(500, "系统异常，请联系管理员");
    }

    public static WXResponseInfo error(int code, String msg) {
        return new WXResponseInfo("error", msg);
    }


    public static WXResponseInfo succ(Object data) {
        return (new WXResponseInfo()).data(data);
    }

    public WXResponseInfo data(Object data) {
        this.data = data;
        return this;
    }

}
