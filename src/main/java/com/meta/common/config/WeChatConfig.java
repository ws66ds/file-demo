package com.meta.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: xieZW
 * @Date: 2021/11/25 19:48
 */
@Configuration
public class WeChatConfig {

    private static String WECHAT_APPID;

    @Value("${wechat.appid}")
    public void setAppId(String appId) {

        WECHAT_APPID = appId;
    }

    public static String getAppId() {

        return WECHAT_APPID;
    }

    private static String WECHAT_APPSECRET;

    @Value("${wechat.appsecret}")
    public void setAppSecret(String appSecret) {

        WECHAT_APPSECRET = appSecret;
    }

    public static String getAppSecret() {

        return WECHAT_APPSECRET;
    }
}
