package com.meta.common.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

/**
 * @Author: xieZW
 * @Date: 2022/3/22 19:36
 */
public class HttpUtils {
    public static final int HTTP_TIMEOUT = 1000000;
    private static Logger log = LoggerFactory.getLogger(HttpUtils.class);

    public HttpUtils() {
    }

    public static void main(String[] args) {
        Object o = sendGetD("https://91porny.com/forum/view/491190");
        System.out.println("o = " + o);
    }


    public static Object sendGetD(String url) {


        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        String result = null;
        try {
            //设置代理
            System.getProperties().setProperty("http.proxyHost", "v117.v2dns.bar");
            System.getProperties().setProperty("http.proxyPort", "2052");
            HttpResponse resp = client.execute(request);

            HttpEntity entity = resp.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity, "UTF-8");//解析返回数据

                System.out.println(result);

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }


    public static String sendGet(String url, String param) {
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = url;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            Proxy proxy = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress("v117.v2dns.bar", 2052));
            URLConnection connection = realUrl.openConnection(proxy);
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            InputStream inputStream = connection.getInputStream();
            in = new BufferedReader(new InputStreamReader(
                    inputStream));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }

}
