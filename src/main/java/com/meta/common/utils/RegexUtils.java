package com.meta.common.utils;

import org.junit.Test;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author: xieZW
 * @Date: 2021/11/25 19:48
 */
public class RegexUtils {

    public static boolean checkEmail(String email) {
        if (StringUtils.isEmpty(email)) {
            return false;
        }
        String regex = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Test
    public void test() {
        System.out.println(RegexUtils.checkEmail("243@21.com"));
        System.out.println(RegexUtils.checkEmail("243@qq.com"));
        System.out.println(RegexUtils.checkEmail("3412#1243@qq.com"));
        System.out.println(RegexUtils.checkEmail("3412&^1243@22.com"));
        System.out.println(RegexUtils.checkEmail("@121243qq.com"));
        System.out.println(RegexUtils.checkEmail("12ww发大水43@qq.com"));
    }


}
