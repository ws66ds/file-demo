package com.meta.common.utils;

import org.assertj.core.util.Lists;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author: xieZW
 * @Date: 2021/11/19 14:45
 * <p>
 * 双色球的工具类
 */
public class DoubleColorUtils {

    public static void main(String[] args) {
        String expect = getExpect("");
        System.out.println("expect = " + expect);
    }


    /**
     * 获取当前中奖号码
     *
     * @param str
     * @return
     * @throws Exception
     */
    public static String getExpect(String str) {

        List<String> list = historyList("21036");
        //获取最近的一期
        String info = list.get(0);
        //时间、期数、号码
        String[] split = info.split(":");
        String time = split[0];
        //与当前时间不是一天，那么不管
        String yyyy = DateUtil.format(new Date(), "yyyy-MM-dd");
        if (yyyy.equals(time)) {
            return split[2];
        }
        return "";
    }


    /**
     * @param url 访问路径
     * @return
     */
    public static Document getDocument(String url) {
        try {
            //5000是设置连接超时时间，单位ms
            return Jsoup.connect(url).timeout(5000).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static List<String> historyList(String startInfo) {
        if (StringUtils.isEmpty(startInfo)) {
            startInfo = "03036";
        }
        List<String> list = Lists.newArrayList();
        //03001代表03年第一期彩票  21036代表21年第36期彩票
        Document doc = getDocument("https://datachart.500.com/ssq/history/newinc/history.php?start=" + startInfo + "&end=33036");
        // 获取目标HTML代码
        Elements trs = doc.select("tbody[id=tdata]").select("tr");
        trs.forEach(e -> {
            Elements tds = e.select("td");
            list.add(tds.get(15).text() + ":" + tds.get(0).text() + ":" + tds.get(1).text() + "," + tds.get(2).text() + "," + tds.get(3).text() + "," + tds.get(4).text() + "," + tds.get(5).text() + "," + tds.get(6).text() + "," + tds.get(7).text());
        });
        return list;
    }


    /**
     * 分析获取最高频率的号码
     * <p>
     * 时间 ： 期数 ： 号码，
     *
     * @param list
     * @return
     */
    public static String analyzeInfo(List<String> list) {

        Map<String, Integer> redMap = new HashMap<>();
        Map<String, Integer> blueMap = new HashMap<>();
        for (String info : list) {
            String[] split = info.split(":");
            String nums = split[2];
            String[] num = nums.split(",");
            for (int i = 0; i < num.length; i++) {
                if (i < num.length - 1) {
                    Integer count = redMap.get(num[i]) == null ? 0 : redMap.get(num[i]);
                    redMap.put(num[i], count + 1);
                } else {
                    Integer count = blueMap.get(num[i]) == null ? 0 : blueMap.get(num[i]);
                    blueMap.put(num[i], count + 1);
                }
            }
        }
        //两级反转
        Map<String, String> redMapTemp = new HashMap<>();
        Map<String, String> blueMapTemp = new HashMap<>();
        Set<Map.Entry<String, Integer>> redentries = redMap.entrySet();
        Set<Map.Entry<String, Integer>> blueentries = blueMap.entrySet();
        for (Map.Entry<String, Integer> entry : redentries) {
            redMapTemp.put(entry.getValue() + "," + entry.getKey(), entry.getKey());
        }
        for (Map.Entry<String, Integer> entry : blueentries) {
            blueMapTemp.put(entry.getValue() + "," + entry.getKey(), entry.getKey());
        }
        //现在已经获取每个号码出现的次数了，接下来是排序
        List<String> redNums = getFinalNums(redMapTemp, 6);
        List<String> blueNums = getFinalNums(blueMapTemp, 1);
        String finalNumStr = redNums.toString() + blueNums.toString();
        return finalNumStr;
    }

    private static List<String> getFinalNums(Map<String, String> redMapTemp, Integer count) {

        Set<Map.Entry<String, String>> entries = redMapTemp.entrySet();
        List<Integer> redList = new ArrayList<>();
        for (Map.Entry<String, String> entry : entries) {
            String key = entry.getKey();
            String[] split = key.split(",");
            Integer countNum = Integer.valueOf(split[0]);
            redList.add(countNum);
        }
        Object[] a = redList.toArray();
        Arrays.sort(a);
        //倒着找count个数字
        List<String> sixRed = new ArrayList<>();
        int sum = 0;
        for (int i = a.length - 1; i >= 0; i--) {
            if (sum < count) {
                sixRed.add(String.valueOf(a[i]));
                sum++;
            }
        }
        //去找到对应的key
        List<String> finalRedNum = new ArrayList<>();
        //去重
        Map<Object, Object> De_duplication = new HashMap<>();
        for (String redNum : sixRed) {
            for (Map.Entry<String, String> entry : entries) {
                if (entry.getKey().contains(redNum)) {
                    if (finalRedNum.size() < count) {
                        //count个数字后就结束
                        if (De_duplication.get(entry.getValue()) == null) {
                            finalRedNum.add(entry.getValue());
                            De_duplication.put(entry.getValue(), entry.getValue());
                        }
                    }
                }
            }
        }
        return finalRedNum;
    }
}
