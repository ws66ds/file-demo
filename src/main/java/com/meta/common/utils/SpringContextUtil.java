//package com.meta.common.utils;
//
//import org.springframework.beans.BeansException;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.stereotype.Service;
//
//import java.util.Map;
//
///**
// *
// */
//@Service
//public class SpringContextUtil implements ApplicationContextAware {
//
//
//    private static ApplicationContext applicationContext;
//    //setApplicationContext方法就是把 ApplicationContext容器的值给你注入进来，实例化后的值注入进来，
//    //ApplicationContext容器:所有spring配置文件中的东西和注解自动注入的东西集合
//
//    /**
//     * 获取applicationContext对象
//     *
//     * @return
//     */
//    public static ApplicationContext getApplicationContext() {
//        return applicationContext;
//    }
//
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        SpringContextUtil.applicationContext = applicationContext;
//    }
//
//    /**
//     * 根据bean的id来查找对象
//     *
//     * @param id
//     * @return
//     */
//    public static Object getBeanById(String id) {
//        return applicationContext.getBean(id);
//    }
//
//    /**
//     * 根据bean的class来查找对象
//     *
//     * @param c
//     * @return
//     */
//    public static <T> T getBeanByClass(Class c) {
//        Object obj = applicationContext.getBean(c);
//        if (obj != null) {
//            return (T) obj;
//        } else {
//            return null;
//        }
//    }
//
//    /**
//     * 根据bean的class来查找所有的对象(包括子类)
//     *
//     * @param c
//     * @return
//     */
//    public static Map getBeansByClass(Class c) {
//        return applicationContext.getBeansOfType(c);
//    }
//}
