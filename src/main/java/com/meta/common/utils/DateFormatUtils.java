package com.meta.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: xieZW
 * @Date: 2021/11/9 16:06
 */
public class DateFormatUtils {
    public static final String DATE_FORMAT_DMY = "dd/MM/yyyy";
    public static final String DATE_FORMAT_YMD = "yyyy-MM-dd";
    public static final String DATE_FORMAT_YM = "yyyy-MM";
    public static final String DATE_FORMAT_Y = "yyyy";
    public static final String DATE_FORMAT_YMDHMS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_YMDHM = "yyyy-MM-dd HH:mm";
    public static final String DATE_FORMAT_MDHM = "MM-dd HH:mm";
    public static final String DATE_NUMBER_FORMAT_YMD = "yyyyMMdd";
    public static final String DATE_NUMBER_FORMAT_YMDHMS = "yyyyMMddHHmmssSS";
    public static final String DATE_NUMBER_FORMAT_YM = "yyyyMM";
    public static final String DATE_NUMBER_FORMAT_Y = "yyyy";
    public static final String DATE_FORMAT_YMD_2 = "yyyy.MM.dd HH:mm:ss";
    public static final String DATE_NUMBER_FORMAT_YMDH = "yyyyMMddHH";
    public static final String DATE_NUMBER_FORMAT_YMDHM = "yyyyMMddHHmm";
    public static final String DATE_FORMAT_YMDHMS_SS = "yyyy-MM-dd HH:mm:ss SS";
    public static final String DATE_FORMAT_YMDHMS_SS_TWO = "yyyy-MM-dd HH:mm:ss.SS";
    public static final String DATE_FORMAT_01 = "yyyy.MM.dd";
    public static final String DATE_FORMAT_YMD_SLASH = "yyyy/MM/dd";
    private static final Logger logger = LoggerFactory.getLogger(DateFormatUtils.class);

    public DateFormatUtils() {
    }

    public static String formatYMDHMS(Date date) {
        if (date == null) {
            return null;
        } else {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return format.format(date);
        }
    }

    public static Date getNowDate(String format) {
        try {
            return charToDate(format(new Date(), format), format);
        } catch (Exception var2) {
            logger.error(var2.getMessage(), var2);
            return null;
        }
    }

    public static Date charToDate(String dateStr, String format) throws Exception {
        if (StringUtils.isEmpty(dateStr)) {
            return null;
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.parse(dateStr);
        }
    }

    public static String format(Date date, String patten) {
        if (date == null) {
            return null;
        } else {
            if (StringUtils.isEmpty(patten)) {
                patten = "yyyy-MM-dd";
            }

            SimpleDateFormat format = new SimpleDateFormat(patten);
            return format.format(date);
        }
    }
}
