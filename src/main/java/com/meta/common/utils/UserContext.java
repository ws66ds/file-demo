package com.meta.common.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: xieZW
 * @Date: 2021/11/25 20:22
 */
public class UserContext {
    private static ThreadLocal threadLocal = new ThreadLocal();
    private HttpServletRequest request;
    private HttpServletResponse response;
    private String userId;
    private String dbName;
    private String corpCode;
    private String corpId;

    private UserContext() {
    }

    private UserContext(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    public static UserContext getCurrentContext() {
        return (UserContext) threadLocal.get();
    }

    public static void setCurrentContext(HttpServletRequest request, HttpServletResponse response) {
        UserContext us = getCurrentContext();
        if (us == null) {
            us = new UserContext(request, response);
            threadLocal.set(us);
        } else {
            us.request = request;
            us.response = response;
        }
    }

    public static void clear() {
        threadLocal.remove();
    }

    public HttpServletRequest getRequest() {
        return this.request;
    }

    public HttpServletResponse getResponse() {
        return this.response;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbName() {
        return this.dbName;
    }

    public void setCorpCode(String corpCode) {
        this.corpCode = corpCode;
    }

    public String getCorpCode() {
        return this.corpCode;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getCorpId() {
        return this.corpId;
    }
}
