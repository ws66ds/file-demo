package com.meta.common.utils;

import lombok.extern.slf4j.Slf4j;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import java.util.Properties;

/**
 * @author ：xzw
 * @date ：Created in 2021/11/18 22:22
 */
@Slf4j
public class SendEmail {

    static  Properties props = new Properties();

    /**
     * ovqfyncousbobbhb
     * @param info neirong
     * @param receiveEmail 收件人邮箱
     *
     * todo  后续可以通过读取配置文件处理
     */

    public static void sendQQEmail(String info, String receiveEmail, String Subject) {

        try {
            // 创建Properties 类用于记录邮箱的一些属性
            // 表示SMTP发送邮件，必须进行身份验证
            props.put("mail.smtp.auth", "true");
            //此处填写SMTP服务器
            props.put("mail.smtp.host", "smtp.qq.com");
            //端口号，QQ邮箱端口587
            props.put("mail.smtp.port", "587");
            // 此处填写，写信人的账号
            props.put("mail.user", "597925798@qq.com");
            // 此处填写16位STMP口令
            props.put("mail.password", "ovqfyncousbobbhb");

            // 构建授权信息，用于进行SMTP进行身份验证
            Authenticator authenticator = new Authenticator() {

                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    // 用户名、密码
                    String userName = props.getProperty("mail.user");
                    String password = props.getProperty("mail.password");
                    return new PasswordAuthentication(userName, password);
                }
            };
            // 使用环境属性和授权信息，创建邮件会话
            Session mailSession = Session.getInstance(props, authenticator);
            // 创建邮件消息
            MimeMessage message = new MimeMessage(mailSession);
            // 设置发件人
            InternetAddress form = new InternetAddress(props.getProperty("mail.user"));
            message.setFrom(form);

            // 设置收件人的邮箱
            InternetAddress to = new InternetAddress(receiveEmail);
            message.setRecipient(RecipientType.TO, to);

            // 设置邮件标题
            message.setSubject(Subject);

            // 设置邮件的内容体
            message.setContent(info, "text/html;charset=UTF-8");

            // 最后当然就是发送邮件啦
            Transport.send(message);
        } catch (MessagingException e) {
            log.error(e.getMessage(),e);
        }

    }
}
