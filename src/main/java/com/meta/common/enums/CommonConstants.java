package com.meta.common.enums;

/**
 * @Author: xieZW
 * @Date: 2021/11/9 16:07
 */
public final class CommonConstants {


    public static String double_color_ball_history_high_num_key = "double_color_ball_history_high_num_key";

    public static final String OSS_COURSE_PATH = "prosity";

    public interface Status {
        int NORMAL = 0;
        int INVALID = 1;
    }

    /**
     * 会员等级
     * 0：普通用户  1：普通会员  2：企业会员
     */
    public interface VipLevel {
        int VISITOR = 0;
        int NORMAL_MEMBER = 1;
        int COMPANY_MEMBER = 2;
    }

    /**
     * banner 类型
     * 1：课程  2：轻应用  3：惠购
     */
    public interface BannerType {
        int COURSE = 1;
        int APPLICATION = 2;
        int DISCOUNT = 3;
        int QING_YING_YONG = 999;
        int YOU_XUAN = 1000;
    }

}
