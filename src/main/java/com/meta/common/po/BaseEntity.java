//package com.meta.common.po;
//
//import lombok.Data;
//
//import javax.persistence.Column;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import java.io.Serializable;
//
///**
// * @Author: xieZW
// * @Date: 2021/11/9 15:37
// */
//@Data
//public class BaseEntity implements Serializable {
//
//    private static final long serialVersionUID = 8885835605865957881L;
//
//    @Id
//    @Column(name = "c_id")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    Long id;
//
//    public BaseEntity(Long id) {
//        this.id = id;
//    }
//
//    public BaseEntity() {
//    }
//}
