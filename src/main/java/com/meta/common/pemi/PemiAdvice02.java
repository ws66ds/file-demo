package com.meta.common.pemi;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.meta.auth.ShiroUtils;
import com.meta.auth.vo.CorpUserInfoVO;
import com.meta.common.vo.ResponseInfo;

/**
 * @Author: xieZW
 * @Date: 2021/12/3 18:10
 */
@Aspect
@Component
public class PemiAdvice02 {

    @Autowired
    private RedisTemplate redisTemplate;

    @Pointcut("@annotation(com.meta.common.pemi.Pemi02)")
    private void permission() {

    }

    @Around("permission()")
    public Object pemiTest(ProceedingJoinPoint point) throws Throwable {

        Subject subject = SecurityUtils.getSubject();
        Object principal = subject.getPrincipal();
        System.out.println("principal = " + principal);
        CorpUserInfoVO userVo = ShiroUtils.getUserVo();
        if (!userVo.getRealName().equals("admin")) {
            return ResponseInfo.error("无权限", "无权限");
        }
        Object proceed = point.proceed();
        return proceed;

    }
}

