package com.meta.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meta.api.param.UserParam;
import com.meta.app.entity.MemberEntity;
import com.meta.app.mapper.MemberMapper;
import com.meta.common.vo.PageInfoVO;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2021/11/25 19:48
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MemberService {

    @Autowired
    private MemberMapper memberMapper;

    public MemberEntity findUser(String unionid) {

        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setUnionId(unionid);
        return memberMapper.selectByUniId(unionid);
    }

    public void addUser(MemberEntity entity) {

        int insert = memberMapper.insertInfo(entity);
    }

    public PageInfoVO queryUserList(MemberEntity param) {

        PageInfoVO pageInfoVO = new PageInfoVO();
        int start = (param.getPage() - 1) * param.getLimit();
        List<MemberEntity> userModels = memberMapper.selectByLimit(param.getNickName(), param.getPhoneNum(), start, param.getLimit());
        List<MemberEntity> all = memberMapper.selectByLimit(param.getNickName(), param.getPhoneNum(), null, null);
        int size = all.size();
        pageInfoVO.setCount(size);
        pageInfoVO.setData(userModels);
        return pageInfoVO;
    }

    public void updateUser(MemberEntity param) {

        int insert = memberMapper.updateUser(param);
    }
}
