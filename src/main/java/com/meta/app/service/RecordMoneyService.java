package com.meta.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meta.app.entity.RecordMoneyEntity;
import com.meta.app.mapper.RecordMoneyMapper;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/1/27 10:38
 */
@Service
public class RecordMoneyService {

    @Autowired
    private RecordMoneyMapper recordMoneyMapper;

    public int add(RecordMoneyEntity recordMoneyEntity) {

        return recordMoneyMapper.add(recordMoneyEntity);
    }

    public List<RecordMoneyEntity> queryRecord(Long userId) {

        return recordMoneyMapper.queryRecord(userId);
    }
}
