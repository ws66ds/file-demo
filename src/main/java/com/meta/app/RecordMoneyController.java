package com.meta.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.meta.api.controller.LoginController;
import com.meta.app.entity.RecordMoneyEntity;
import com.meta.app.service.RecordMoneyService;
import com.meta.common.vo.ResponseInfo;
import java.util.Date;

/**
 * @Author: xieZW
 * @Date: 2022/1/27 10:31
 */
@RestController
@RequestMapping("app-money")
public class RecordMoneyController {

    private static Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RecordMoneyService recordMoneyService;

    @PostMapping("addRecord")
    public ResponseInfo add(@RequestBody JSONObject jsonObject) {

        //消费类型
        String type = (String) jsonObject.get("email");
        String token = (String) jsonObject.get("token");
        //消费钱
        String money = (String) jsonObject.get("msg");
        if (token == null) {
            return ResponseInfo.errorWX("获取用户信息失败，请重新登录！");
        }
        //user info
        JSONObject o = (JSONObject) redisTemplate.opsForValue().get(token);
        RecordMoneyEntity recordMoneyEntity = new RecordMoneyEntity();
        recordMoneyEntity.setCreateTime(new Date());
        recordMoneyEntity.setMoney(money);
        recordMoneyEntity.setType(type);
        recordMoneyEntity.setUpdateTime(new Date());
        Object userId = o.get("userId");
        recordMoneyEntity.setUserId(Long.valueOf(String.valueOf(userId)));
        int add = recordMoneyService.add(recordMoneyEntity);
        if (add == 1) {
            return ResponseInfo.succ("保存成功!");
        } else {
            return ResponseInfo.error("保存失败!");
        }
    }


}
