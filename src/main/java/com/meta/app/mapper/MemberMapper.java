package com.meta.app.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.meta.app.entity.MemberEntity;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2021/11/25 19:48
 */
@Component
public interface MemberMapper {

    MemberEntity selectByUniId(@Param("unionid") String unionid);

    int insertInfo(MemberEntity entity);

    List<MemberEntity> selectByLimit(@Param("username") String username, @Param("mobile") String mobile, @Param("start") Integer start, @Param("limit") Integer limit);

    int updateUser(@Param("param") MemberEntity param);
}
