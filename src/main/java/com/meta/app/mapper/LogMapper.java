package com.meta.app.mapper;

import org.springframework.stereotype.Component;

import com.meta.app.entity.LogEntity;

/**
 * @Author: xieZW
 * @Date: 2021/11/26 17:36
 */
@Component
public interface LogMapper {

    /**
     * 增加日志
     *
     * @param logEntity
     * @return
     */
    Integer insertInfo(LogEntity logEntity);
}
