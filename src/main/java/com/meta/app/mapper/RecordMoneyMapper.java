package com.meta.app.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.meta.app.entity.RecordMoneyEntity;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/1/27 10:38
 */
@Component
public interface RecordMoneyMapper {

    /**
     * 新增
     *
     * @param entity
     * @return int
     */
    int add(@Param("entity") RecordMoneyEntity entity);

    /**
     * 查询
     *
     * @param userId
     * @return
     */
    List<RecordMoneyEntity> queryRecord(@Param("userId") Long userId);
}
