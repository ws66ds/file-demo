package com.meta.app.entity;

import lombok.Data;

import com.meta.param.IParam;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Author: xieZW
 * @Date: 2021/11/25 19:48
 */
@Data
@Entity
public class BaseEntity extends IParam implements Serializable {

    private static final long serialVersionUID = 8885835605865957881L;

    @Id
    @Column(name = "c_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

}
