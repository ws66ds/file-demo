package com.meta.app.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Author: xieZW
 * @Date: 2021/11/25 19:48
 */
@Data
@Table(name = "T_CORP_USER_INFO")
public class MemberEntity extends SimpleUser {

    private static final long serialVersionUID = -3190379488940038910L;

    @Column(name = "C_WECHAT_UNIONID")
    private String unionId;

    @Column(name = "C_WECHAT_OPENID")
    private String openId;

    @Column(name = "C_PASSWORD")
    private String password;

    @Column(name = "C_POINTS_NUM")
    private Integer pointsNum;

    @Column(name = "C_VIP_LEVEL")
    private Integer vipLevel;

    @Column(name = "C_REGISTER_DATE")
    private Date registerDate;

    /**
     * 0-游客 1-普通会员 2-企业会员 3-企业会员管理员
     */
    @Column(name = "C_STATUS")
    private Integer status;

    @Column(name = "C_UPDATE_DATE")
    private Date updateDate;
}
