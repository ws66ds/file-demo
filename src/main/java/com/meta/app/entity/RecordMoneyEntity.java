package com.meta.app.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Author: xieZW
 * @Date: 2022/1/27 10:38
 */
@Data
@Table(name = "t_record_money")
public class RecordMoneyEntity extends BaseEntity {

    @Column(name = "c_userId")
    private Long userId;
    @Column(name = "c_type")
    private String type;
    @Column(name = "c_money")
    private String money;
    @Column(name = "c_create_date")
    private Date createTime;
    @Column(name = "c_update_date")
    private Date updateTime;
}
