package com.meta.app.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Transient;
import java.util.Date;

/**
 * @Author: xieZW
 * @Date: 2021/11/25 19:48
 */
@Data
public class SimpleUser extends BaseEntity {

    private static final long serialVersionUID = 8204466511304325832L;

    @Column(name = "C_WECHAT_AVATAR")
    private String avatar;

    @Column(name = "C_NICK_NAME")
    private String nickName;

    private String chatResult;

    @Column(name = "C_REAL_NAME")
    private String realName;

    @Column(name = "C_PHONE_NUM")
    private String phoneNum;

    @Column(name = "C_EMAIL")
    private String email;

    @Column(name = "C_COMPANY")
    private String company;

    @Column(name = "C_POSITION")
    private String position;

    @Column(name = "C_CORP_ID")
    private Long corpId;

    @Column(name = "C_BUY_VIP_DATE")
    private Date buyVipDate;

    @Column(name = "C_END_VIP_DATE")
    private Date endVipDate;

    @Column(name = "C_ADMIN")
    private Integer admin;

    @Transient
    String code;

    @Transient
    String vcode;

    @Transient
    String shareId;

    /**
     * 申请的企业会员等级
     * todo
     */
    @Transient
    Integer grade;

}
