package com.meta.app.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Date;

/**
 * @Author: xieZW
 * @Date: 2021/11/25 19:48
 */
@Data
@Table(name = "t_log")
public class LogEntity extends BaseEntity {

    @Column(name = "c_content")
    private String content;

    @Column(name = "c_date")
    private Date date;
}
