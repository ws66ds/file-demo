package com.meta.readpicinfo;

import java.util.HashMap;

/**
 * @Author: xieZW
 * @Date: 2022/10/12 17:49
 */
public class ConstansKeJi {


    static HashMap<Object, Object> map = null;

    static {
        map = new HashMap<>();
        map.put("苯甲酸", "benjiasuan");
        map.put("山梨酸", "shanlisuan");
        map.put("丙酸", "bingsuan");
        map.put("对羟基苯甲", "duiqingjiabenjia");
        map.put("那他霉素", "natameisu");
        map.put("葡萄糖氧化酶", "putaotanganhuamei");
        map.put("鱼精蛋白", "yujingdanbai");
        map.put("溶菌酶", "rongjunmei");
        map.put("聚赖氨酸", "julaiansuan");
        map.put("壳聚糖", "kejutang");
        map.put("果胶分解物", "guojiaofenjiewu");
        map.put("蜂胶", "fengjiao");
        map.put("茶多酚", "chaduofen");
        map.put("乳化剂", "ruhuaji");
        map.put("脂肪酸甘油酯", "zhifangsuanganyouzhi");
        map.put("醋酸视黄酯", "cusuanshiguangzhi");
        map.put("牛磺酸", "niuhuangsuan");
    }


    static HashMap<Object, Object> mapInfo = null;

    static {
        mapInfo = new HashMap<>();
        mapInfo.put("benjiasuan", "苯甲酸又称安息香酸，是一种常用的有机杀菌剂。在pH值低的环境中，苯甲酸对广范围的微生物有效，但对产酸菌作用弱。当pH高于5.5时，对很多霉菌和酵母没有效果。苯甲酸抑菌的最适宜pH值为2.5-4，对一般微生物的完全抑制最小质量分数为0.05％～0.1％");
        mapInfo.put("shanlisuan", "山梨酸化学名为2，4一己二烯酸，是一种广谱食品防腐剂");
        mapInfo.put("bingsuan", "丙酸是具有类似醋酸刺激酸香的液体，由于是人体新陈代谢的正常中间物，故无毒性，其ADI值不加限制。丙酸对霉菌、好气性细菌、革兰氏阴性菌，尤其是对使面包生成丝状黏质的大肠芽孢杆菌有效，并能防止黄曲霉毒素的产生，所以常用于面包及糕点的制作。丙酸盐具有相同的防腐效果，常用的是钙盐和钠盐");
        mapInfo.put("duiqingjiabenjia", "对羟基苯甲酸酯又称尼泊金酯，为无色结晶或白色结晶粉末，无味，无臭。主要用于酱油、果酱、清凉饮料等。防腐效果优于苯甲酸及其钠盐，使用量约为苯甲酸钠的1/10，适宜pH值为4-8。对羟基苯甲酸酯的毒性低于苯甲酸，其水溶性较差，常用醇类先溶解后再使用，价格也较高");
        mapInfo.put("natameisu", "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值");
        mapInfo.put("putaotanganhuamei", "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值");
        mapInfo.put("yujingdanbai", "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值");
        mapInfo.put("rongjunmei", "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值");
        mapInfo.put("julaiansuan", "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值");
        mapInfo.put("kejutang", "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值");
        mapInfo.put("guojiaofenjiewu", "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值");
        mapInfo.put("fengjiao", "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值");
        mapInfo.put("chaduofen", "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值");
        mapInfo.put("ruhuaji", "凡是添加少量即能使互不相溶的液体(如油和水)形成稳定乳浊液的食品添加剂称为乳化剂");
        mapInfo.put("zhifangsuanganyouzhi", "甘油和脂肪酸反应，可以生成单、双和三酯。单脂肪酸甘油酯，简称单甘酯，是一种重要的食品乳化剂，广泛用于起酥油、糕点、面包、糖果、冰淇淋中，起乳化、发泡、防结晶、抗老化作用");
        mapInfo.put("cusuanshiguangzhi", "这是测试数据!");
        mapInfo.put("niuhuangsuan", "里面含有牛磺酸!");
    }


//    public static String benjiasuan = "苯甲酸又称安息香酸，是一种常用的有机杀菌剂。在pH值低的环境中，苯甲酸对广范围的微生物有效，但对产酸菌作用弱。当pH高于5.5时，对很多霉菌和酵母没有效果。苯甲酸抑菌的最适宜pH值为2.5-4，对一般微生物的完全抑制最小质量分数为0.05％～0.1％";
//    public static String shanlisuan = "山梨酸化学名为2，4一己二烯酸，是一种广谱食品防腐剂";
//    public static String bingsuan = "丙酸是具有类似醋酸刺激酸香的液体，由于是人体新陈代谢的正常中间物，故无毒性，其ADI值不加限制。丙酸对霉菌、好气性细菌、革兰氏阴性菌，尤其是对使面包生成丝状黏质的大肠芽孢杆菌有效，并能防止黄曲霉毒素的产生，所以常用于面包及糕点的制作。丙酸盐具有相同的防腐效果，常用的是钙盐和钠盐";
//    public static String duiqingjiabenjia = "对羟基苯甲酸酯又称尼泊金酯，为无色结晶或白色结晶粉末，无味，无臭。主要用于酱油、果酱、清凉饮料等。防腐效果优于苯甲酸及其钠盐，使用量约为苯甲酸钠的1/10，适宜pH值为4-8。对羟基苯甲酸酯的毒性低于苯甲酸，其水溶性较差，常用醇类先溶解后再使用，价格也较高";
//    public static String natameisu = "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值";
//    public static String putaotanganhuamei = "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值";
//    public static String yujingdanbai = "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值";
//    public static String rongjunmei = "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值";
//    public static String julaiansuan = "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值";
//    public static String kejutang = "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值";
//    public static String guojiaofenjiewu = "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值";
//    public static String fengjiao = "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值";
//    public static String chaduofen = "天然防腐剂具有抗菌性强、安全无毒、水溶性好、热稳定性好、作用范围广等优点，不但对人体健康无害，而且还具有一定的营养价值";
//    public static String ruhuaji = "凡是添加少量即能使互不相溶的液体(如油和水)形成稳定乳浊液的食品添加剂称为乳化剂";
//    public static String zhifangsuanganyouzhi = "甘油和脂肪酸反应，可以生成单、双和三酯。单脂肪酸甘油酯，简称单甘酯，是一种重要的食品乳化剂，广泛用于起酥油、糕点、面包、糖果、冰淇淋中，起乳化、发泡、防结晶、抗老化作用";


}
