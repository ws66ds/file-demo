package com.meta.readpicinfo;

import lombok.Data;

import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/10/12 17:43
 */
@Data
public class ResultVO {

    private List<WordsResultVO> words_result;
    private Object log_id;
    private Object words_result_num;
    private Object direction;
}
