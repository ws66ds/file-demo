package com.meta.readpicinfo;

import lombok.Data;

/**
 * @Author: xieZW
 * @Date: 2022/10/12 18:17
 */
@Data
public class WordsResultVO {

    private ProbabilityVO probability;
    private String words;
}
