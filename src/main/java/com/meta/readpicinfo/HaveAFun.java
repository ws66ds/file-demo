package com.meta.readpicinfo;

import org.json.JSONObject;
import org.springframework.util.StringUtils;

import com.baidu.aip.ocr.AipOcr;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author: xieZW
 * @Date: 2022/10/2 22:14
 */
public class HaveAFun {

    public static void main(String[] args) {
        // 初始化一个AipOcr
        AipOcr client = new AipOcr("27874992", "bGRAyxFiswxZki22EXRS6Tfl", "tN55UZTuWPV9e2gdh9XshGEGiKyMzLIb");

        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("detect_direction", "true");
        options.put("probability", "true");

        // 调用通用文字识别（高精度版）接口
        String path = "E:\\ws66ds\\baiduyuninfo\\20221012112758.jpg";
        JSONObject res = client.basicAccurateGeneral(path, options);
        System.out.println(res);
        String x = res.toString();
        System.out.println(x);

        ResultVO resultVO = com.alibaba.fastjson.JSONObject.parseObject(x, ResultVO.class);
        System.out.println("resultVO = " + resultVO.toString());

        StringBuffer sb = new StringBuffer();
        List<WordsResultVO> words_result = resultVO.getWords_result();
        for (WordsResultVO jsonObject : words_result) {
            String words = jsonObject.getWords();
            sb.append(words);
        }

        System.out.println("words = " + words_result.toString());

        String result = sb.toString();

        HashMap<Object, Object> map = ConstansKeJi.map;
        Set<Map.Entry<Object, Object>> entries = map.entrySet();

        StringBuffer resultInfo = new StringBuffer();
        for (Map.Entry<Object, Object> entry : entries) {
            Object key = entry.getKey();
            if (result.contains(key.toString())) {
                Object value = entry.getValue();
                Object info = ConstansKeJi.mapInfo.get(value);
                resultInfo.append(info).append("\n");
            }
        }
        if (StringUtils.isEmpty(resultInfo.toString())) {
            resultInfo.append("很健康，放心吃，哥们儿~");
        } else {
            resultInfo.append("唉呀妈呀，一勺三花蛋奶，头发掉光光，老妹儿~");
        }
        System.out.println("打印结果：");
        System.out.println(resultInfo.toString());
    }

}
