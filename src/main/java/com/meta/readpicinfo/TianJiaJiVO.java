package com.meta.readpicinfo;

import lombok.Data;

/**
 * @Author: xieZW
 * @Date: 2022/10/14 22:18
 */
@Data
public class TianJiaJiVO {

    private String name;
    private String type;
    private String desc;
    private String remarks;
    private String url;
}
