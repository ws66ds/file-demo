package com.meta.readpicinfo;

import lombok.Data;

/**
 * @Author: xieZW
 * @Date: 2022/10/12 18:18
 */
@Data
public class ProbabilityVO {

    private Object average;
    private Object min;
    private Object variance;
}
