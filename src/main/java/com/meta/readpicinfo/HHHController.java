package com.meta.readpicinfo;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.baidu.aip.ocr.AipOcr;
import com.meta.auth.utils.ResourceUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Author: xieZW
 * @Date: 2022/10/12 18:37
 */
@RestController
@RequestMapping("app/hhhhh")
@Slf4j
public class HHHController {

    private static List<TianJiaJiVO> list = new ArrayList<>();

    static {
        list = getPemiConfigFromResource();
    }

    private static List<TianJiaJiVO> getPemiConfigFromResource() {
        List<TianJiaJiVO> totalList = new ArrayList<>();
        totalList = ResourceUtils.readConfigList("config/tianjiaji.json", TianJiaJiVO.class);
        return totalList;
    }

    public static <E> List<E> readConfigList(String pathAndFileName, Class<E> clazz) {
        String json = ResourceUtils.resource2String(pathAndFileName);
        return JSON.parseArray(json, clazz);
    }

    public static String resource2String(String pathAndFileName) {
        InputStream in = getInputStream(pathAndFileName);
        if (in == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"))) {
            String temp;
            while ((temp = br.readLine()) != null) {
                builder.append(temp).append("\n");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return builder.toString();
    }

    public static InputStream getInputStream(String pathAndFileName) {
        InputStream in = ResourceUtils.class.getClassLoader().getResourceAsStream(pathAndFileName);
        if (in == null) {
            in = ClassLoader.getSystemResourceAsStream(pathAndFileName);
        }
        if (in == null) {
            return null;
        }
        return in;
    }

    @PostMapping("xxxx")
    public List<TianJiaJiVO> uploadFileToOss(@RequestParam("file") MultipartFile fileCnt) {

        String contentType = fileCnt.getContentType();
        if (!ACCEPT_COURSE_IMAGE_FILE_TYPE.contains(contentType)) {
            throw new RuntimeException("非图片，不能解析");
//            return ResponseInfo.error("请上传图片，老妹儿~");
        }

        AipOcr client = new AipOcr("27874992", "bGRAyxFiswxZki22EXRS6Tfl", "tN55UZTuWPV9e2gdh9XshGEGiKyMzLIb");

        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("detect_direction", "true");
        options.put("probability", "true");
        StringBuffer resultInfo = new StringBuffer();
        List<TianJiaJiVO> resultList = new ArrayList<>();

        try {
            byte[] bytes = fileCnt.getBytes();
            JSONObject res = client.basicAccurateGeneral(bytes, options);

            System.out.println(res);
            String x = res.toString();
            System.out.println(x);

            ResultVO resultVO = com.alibaba.fastjson.JSONObject.parseObject(x, ResultVO.class);
            System.out.println("resultVO = " + resultVO.toString());

            StringBuffer sb = new StringBuffer();
            List<WordsResultVO> words_result = resultVO.getWords_result();
            for (WordsResultVO jsonObject : words_result) {
                String words = jsonObject.getWords();
                sb.append(words);
            }

            System.out.println("words = " + words_result.toString());

            String result = sb.toString();


            List<TianJiaJiVO> list22 = new ArrayList<>();
            for (TianJiaJiVO tianJiaJiVO : HHHController.list) {
                String name = tianJiaJiVO.getName();
                if (result.contains(name)) {
                    if (name.equals("山梨酸")) {
                        if (result.contains("山梨酸,") || result.contains("山梨酸，") || result.contains("山梨酸、") || result.contains("山梨酸。")) {
                            list22.add(tianJiaJiVO);
                        } else {
                            continue;
                        }
                    }
                    list22.add(tianJiaJiVO);
                }
            }
            if (list22.size() > 0) {
                resultInfo.append("共检测出" + list22.size() + "种可能有副作用添加剂").append("\n");
            }
            for (int i = 0; i < list22.size(); i++) {
                TianJiaJiVO tianJiaJiVO = list22.get(i);
                String name = tianJiaJiVO.getName();
                resultList.add(tianJiaJiVO);
                resultInfo.append("第" + (i + 1) + "种添加剂是：").append(name).append(":").append(tianJiaJiVO.getRemarks()).append("\n");
            }

            if (StringUtils.isEmpty(resultInfo.toString())) {
                resultInfo.append("很健康，放心吃，哥们儿~");
            } else {
                resultInfo.append("唉呀妈呀，一勺三花蛋奶，头发掉光光，老妹儿~");
            }
            System.out.println("打印结果：");
            System.out.println(resultInfo.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultList;
    }


    private static final String MIMETYPE_IMAGE_PNG = "image/png";
    private static final String MIMETYPE_IMAGE_JPG = "image/jpg";
    private static final String MIMETYPE_IMAGE_JPEG = "image/jpeg";
    private static final String MIMETYPE_IMAGE_PJPEG = "image/pjpeg";

    public static Set<String> ACCEPT_COURSE_IMAGE_FILE_TYPE = new HashSet<String>(
            4);

    static {
        ACCEPT_COURSE_IMAGE_FILE_TYPE.add(MIMETYPE_IMAGE_PNG);
        ACCEPT_COURSE_IMAGE_FILE_TYPE.add(MIMETYPE_IMAGE_JPG);
        ACCEPT_COURSE_IMAGE_FILE_TYPE.add(MIMETYPE_IMAGE_JPEG);
        ACCEPT_COURSE_IMAGE_FILE_TYPE.add(MIMETYPE_IMAGE_PJPEG);
    }
}
