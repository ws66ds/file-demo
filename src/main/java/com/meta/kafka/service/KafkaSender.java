package com.meta.kafka.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @Author: xieZW
 * @Date: 2022/1/21 13:48
 */
@Component
public class KafkaSender {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void messageSender(String topic, String key, String message) {
        try {
            logger.info("kafka生产者");
            kafkaTemplate.send(topic, key, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
