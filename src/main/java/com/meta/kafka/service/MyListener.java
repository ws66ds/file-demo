package com.meta.kafka.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;

import com.alibaba.fastjson.JSONObject;
import com.meta.common.utils.SendEmail;

/**
 * @Author: xieZW
 * @Date: 2022/1/21 13:48
 */
public class MyListener {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());


    @KafkaListener(topics = {"sendEmail"})
    public void listen(ConsumerRecord<?, ?> record) {

        try {
            logger.info("kafka消费者发送短信");
            logger.info("收到消息的key: " + record.key());
            Object value1 = record.value();
            logger.info("收到消息的value: " + value1.toString());
            JSONObject value = JSONObject.parseObject(value1.toString());
            SendEmail.sendQQEmail(value.get("nums").toString(), value.get("email").toString(), value.get("title").toString());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
        }
    }
}
