package com.meta.kafka.controller;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.meta.kafka.service.KafkaSender;
import java.util.UUID;

/**
 * @Author: xieZW
 * @Date: 2022/1/24 13:48
 */
@RestController
@RequestMapping("/kafka")
public class KafkaController {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private KafkaSender kafkaSender;

    @RequestMapping(value = "/send")
    public void sendKafka(@RequestParam(required = false) String key,
                          @RequestParam(required = false) String value) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject();
            jsonObject.put("title", "往期最高频率数字");
            jsonObject.put("nums", "123");
            jsonObject.put("email", "597925798@qq.com");
            logger.info("kafka的消息={}", value);
            kafkaSender.messageSender("sendEmail", UUID.randomUUID().toString(), jsonObject.toString());
        } catch (Exception e) {
            logger.error("发送kafka异常：", e);
        }
    }
}
