package com.meta.clown;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @Author: xieZW
 * @Date: 2022/1/22 20:17
 * <p>
 * make sure zhe listener how works
 */
@Component
public class AlwaysListen {

    private static Logger log = LoggerFactory.getLogger(AlwaysListen.class);

    @PostConstruct
    public void init() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (log.isDebugEnabled()) {
                        log.debug("listen on zhe way");
                    }
                    //sleep 20 seconds
                    try {
                        Thread.sleep(20000);
                    } catch (Exception e) {
                        if (log.isDebugEnabled()) {
                            log.debug("e.getMessage = {}", e.getMessage());
                            log.debug("e = ,{}" + e);
                        }
                    }
                }
            }
        }).start();
    }
}
