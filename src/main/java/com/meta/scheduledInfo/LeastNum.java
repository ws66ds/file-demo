package com.meta.scheduledInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author: xieZW
 * @Date: 2021/12/1 14:18
 * <p>
 * 最少出现的号码
 */
public class LeastNum {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>(10000);
        for (int i = 1000000; i > 0; i--) {
            String colorNum = DoubleColorBall.getColorNum();
            Integer integer = map.get(colorNum);
            if (null == integer) {
                map.put(colorNum, 0);
            } else {
                map.put(colorNum, integer++);
            }
        }

        Set<String> set = map.keySet();
        String str = "";
        Integer count = 0;
        for (String key : set) {
            Integer integer = map.get(key);
            if (integer > count) {
                str = key;
                count = integer;
            }
        }
        System.out.println("str = " + str);
        System.out.println("count = " + count);
    }
}



























