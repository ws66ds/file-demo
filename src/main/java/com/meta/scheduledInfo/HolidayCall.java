package com.meta.scheduledInfo;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.meta.common.utils.HttpUtils;
import com.meta.common.utils.SendEmail;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author: xieZW
 * @Date: 2022/8/26 17:32
 */
@Component
public class HolidayCall {

    /**
     * 发邮箱往期最高频率数字
     * <p>
     */
    @Scheduled(cron = "0 0 8 * * ?")
//    @Scheduled(cron = "0 7 18 * * ?")
    public void setHigNumRedis() {
        Calendar instance = Calendar.getInstance();
        Date time = instance.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(time);
        String httpUrl = "http://timor.tech/api/holiday/info/" + format;
        String str = (String) HttpUtils.sendGetD(httpUrl);
        JSONObject jsonObject = JSONObject.parseObject(str);
        Object holiday = jsonObject.get("holiday");
        if (holiday != null) {
            jsonObject = JSONObject.parseObject(holiday.toString());
            Object name = jsonObject.get("name");
            if (avoid.contains(name.toString())) {
                return;
            }
            for (String o : list) {

                SendEmail.sendQQEmail(name + "快乐~", o, name + "快乐");
            }
        }
    }

    static ArrayList<String> list = new ArrayList();
    static ArrayList<String> avoid = new ArrayList();

    static {
        list.add("597925798");
        list.add("2539104894");


        avoid.add("清明节");
        avoid.add("重阳节");
        avoid.add("端午节");
    }
}
