package com.meta.scheduledInfo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import com.meta.common.utils.DateUtil;
import com.meta.common.utils.SendEmail;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/1/13 15:06
 * <p>
 * 该给电瓶车充电了
 */
@Component
@Slf4j
public class FeedCarSomeE {


    static List<String> emailList = new ArrayList();
    static final List<Integer> dayIndexList = new ArrayList();

    static {
        emailList.add("597925798@qq.com");
        emailList.add("1151205670@qq.com");
        dayIndexList.add(2);
        dayIndexList.add(4);
        dayIndexList.add(7);
    }


    /**
     * 发邮箱往期最高频率数字
     * <p>
     */
//    @Scheduled(cron = "20 20 20 * * ?")
    public void setHigNumRedis() {

        if (!DateUtil.isTheDayInWeekList(dayIndexList)) {
            return;
        }
        log.info("吼吼");
        for (String email : emailList) {
            log.info("吼吼，{}", email);
            SendEmail.sendQQEmail("该给电瓶车充电了！！！！", email, "该充电了");
        }
    }

}

