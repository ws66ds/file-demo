package com.meta.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meta.common.utils.IPutils;
import com.meta.common.vo.ResponseInfo;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


/**
 * @Author: xieZW
 * @Date: 2021/11/15 10:07
 */
@RestController
@RequestMapping("caipiao")
@Slf4j
public class CaiPiaoController {

    @Autowired
    private RedisTemplate redisTemplate;

    static String caipiao_key = "CAI_PIAO_KEY_001";

    static String[] redChar = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
            "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
            "31", "32", "33"};
    static String[] bulueChar = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
            "11", "12", "13", "14", "15", "16"};


    /**
     * 获取 从redis中  根据key信息获取数据
     * http://127.0.0.1:8080/caipiao/getInfo?key=CAI_PIAO_KEY_0010
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/getInfo")
    public synchronized ResponseInfo Test02(String key) {

        ValueOperations valueOperations = redisTemplate.opsForValue();
        String info = (String) valueOperations.get(key);
        redisTemplate.delete(key);
        return ResponseInfo.succ(info);
    }


    /**
     * 生成 存入redis中 返回所有的key信息
     * http://127.0.0.1:8080/caipiao/createInfo
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/createInfo")
    public ResponseInfo Test01(HttpServletRequest request) {


        String ip = IPutils.getIp(request);

        System.out.println("xczxczxc哈哈哈" + Charset.defaultCharset());

        List<String> list = new ArrayList<>();
        int id = 0;
        ValueOperations valueOperations = redisTemplate.opsForValue();
        List<String> redList = new ArrayList<>();
        for (String c : redChar) {
            redList.add(c);
        }
        List<String> blueList = new ArrayList<>();
        for (String c : bulueChar) {
            blueList.add(c);
        }

        list.add(caipiao_key + id + ip);
        valueOperations.set(caipiao_key + id + ip, "八荒六合，唯我独尊");
        id++;
        System.out.println("八荒六合，唯我独尊");
        String TenMillionBonusNum = "";
        List<Integer> nums = new ArrayList<>();
        Random r = new Random();

        for (int i = 32; i > 26; i--) {
            list.add(caipiao_key + id + ip);
            valueOperations.set(caipiao_key + id + ip, "现在抽取您的第" + (33 - i) + "个吉祥数字     ");
            id++;
            System.out.print("现在抽取您的第" + (33 - i) + "个吉祥数字     ");
            int index = r.nextInt(i);
            String num = redList.get(index);
            list.add(caipiao_key + id + ip);
            valueOperations.set(caipiao_key + id + ip, "第" + (33 - i) + "个吉祥数字: " + num);
            id++;
            System.out.println("第" + (33 - i) + "个吉祥数字: " + num);
            redList.remove(index);
            nums.add(Integer.valueOf(num));
        }

        Object[] a = nums.toArray();
        Arrays.sort(a);
        System.out.println("nums = " + nums);
        for (Object o : a) {
            TenMillionBonusNum = TenMillionBonusNum + " " + o + " ";
        }
        list.add(caipiao_key + id + ip);
        valueOperations.set(caipiao_key + id + ip, "现在抽取您的第" + 7 + "个吉祥数字     ");
        id++;
        System.out.print("现在抽取您的第" + 7 + "个吉祥数字     ");
        int index = r.nextInt(16);
        String num = blueList.get(index);
        list.add(caipiao_key + id + ip);
        valueOperations.set(caipiao_key + id + ip, "第" + 7 + "个吉祥数字: " + num);
        id++;
        System.out.println("第" + 7 + "个吉祥数字: " + num);
        nums.add(Integer.valueOf(num));
        TenMillionBonusNum = TenMillionBonusNum + " " + num + " ";
        System.out.println("恭喜主人喜中1000万RMB，您的兑奖号码为：" + TenMillionBonusNum);
        list.add(caipiao_key + id + ip);

        valueOperations.set(caipiao_key + id + ip, "恭喜主人喜中1000万RMB，您的兑奖号码为：" + TenMillionBonusNum);
        id++;
        return ResponseInfo.succ(list);
    }
}
