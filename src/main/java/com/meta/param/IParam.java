package com.meta.param;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: xieZW
 * @Date: 2021/11/9 15:08
 */
@Data
public class IParam implements Serializable {

    private int page;

    private int limit;
}
