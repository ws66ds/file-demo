package com.meta.ddd.entity;

import lombok.Getter;

import com.meta.auth.SpringContextUtil;
import com.meta.ddd.Enums.GenderEnum;
import com.meta.ddd.acl.UserAcl;
import com.meta.ddd.dto.UserDTO;
import com.meta.ddd.repository.UserRepository;

//import com.meta.common.utils.SpringContextUtil;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 14:55
 * <p>
 * 实体层
 * 只提供Get方法，不提供Set方法
 * <p>
 * 其他字段省略
 */
@Getter
public class UserEntity extends IEntity {

    private static final String empId = null;

    /**
     * 用户ID
     */
    private UserId userId;

    /**
     * 性别
     */
    private GenderEnum gender;

    /**
     * 电话
     */
    private MobileEntity mobile;

    /**
     * 邮箱
     */
    private EmailEntity email;

    /**
     * 构建用户模型
     *
     * @param gender
     * @param mobile
     * @param email
     */
    public UserEntity(Integer gender, String mobile, String email) {

        UserRepository userRepository = SpringContextUtil.getBeanByClass(UserRepository.class);
        if (null != userRepository.queryByMobileNull(mobile)) {
            throw new RuntimeException("该用户已存在！");
        }
        this.userId = new UserId(empId);
        this.gender = GenderEnum.getById(gender);
        this.mobile = new MobileEntity(mobile);
        this.email = new EmailEntity(email);
        this.AddUserEntity();
        this.SendMsg();
    }

    /**
     * 添加用户
     *
     * @return
     */
    public UserDTO AddUserEntity() {

        UserRepository userRepository = SpringContextUtil.getBeanByClass(UserRepository.class);
        userRepository.add(this);
        return new UserDTO();
    }

    /**
     * 发送激活短信
     *
     * @return
     */
    public UserDTO SendMsg() {

        UserAcl userAcl = SpringContextUtil.getBeanByClass(UserAcl.class);
        userAcl.SendMsg(this.mobile.getMobile());
        return new UserDTO();
    }
}
