package com.meta.ddd.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 15:01
 */
@Getter
@Setter
public class IEntity implements Serializable {
}
