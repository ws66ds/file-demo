package com.meta.ddd.entity;

import org.springframework.util.StringUtils;

import java.util.UUID;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 15:40
 */
public class IID {

    private String id;

    public String getId() {

        return StringUtils.isEmpty(id) ? UUID.randomUUID().toString() : this.id;
    }

    public IID(String id) {
        this.id = id;
    }
}
