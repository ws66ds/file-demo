package com.meta.ddd.entity;

import lombok.Getter;

import com.meta.ddd.utils.FixUtils;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 15:13
 */
@Getter
public class MobileEntity {

    private String mobile;

    public boolean fixMobile() {

        return FixUtils.FixEmail(this.mobile);
    }

    public MobileEntity(String mobile) {

        this.mobile = mobile;
        if (!fixMobile()) {
            throw new RuntimeException("手机号码不正确！");
        }
    }
}
