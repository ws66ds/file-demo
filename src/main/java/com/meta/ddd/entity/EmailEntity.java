package com.meta.ddd.entity;

import lombok.Getter;

import com.meta.ddd.utils.FixUtils;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 15:13
 */
@Getter
public class EmailEntity {

    private String email;

    public boolean fixEmail() {

        return FixUtils.FixEmail(this.email);
    }

    public EmailEntity(String email) {

        this.email = email;
        if (!fixEmail()) {
            throw new RuntimeException("邮箱格式不正确！");
        }
    }
}
