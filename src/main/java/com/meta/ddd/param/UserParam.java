package com.meta.ddd.param;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 15:37
 */
@Getter
@Setter
public class UserParam extends IParam {

    private String id;
    private Integer gender;
    private String mobile;
    private String email;
}
