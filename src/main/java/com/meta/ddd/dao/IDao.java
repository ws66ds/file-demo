package com.meta.ddd.dao;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 15:22
 */
public interface IDao<T> {

    /**
     * 新增
     *
     * @param t
     * @return
     */
    T add(T t);

    /**
     * 删除
     *
     * @param t
     * @return
     */
    T delete(T t);

    /**
     * 查询
     *
     * @param t
     * @return
     */
    T query(T t);

    /**
     * 修改
     *
     * @param t
     * @return
     */
    T update(T t);
}
