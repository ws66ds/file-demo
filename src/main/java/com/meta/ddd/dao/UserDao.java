package com.meta.ddd.dao;

import org.springframework.stereotype.Component;

import com.meta.ddd.po.UserPO;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 15:22
 */
@Component
public class UserDao implements IDao<UserPO> {

    @Override
    public UserPO add(UserPO userPO) {
        return new UserPO();
    }

    @Override
    public UserPO delete(UserPO userPO) {
        return null;
    }

    @Override
    public UserPO query(UserPO userPO) {
        return null;
    }

    @Override
    public UserPO update(UserPO userPO) {
        return null;
    }
}
