package com.meta.ddd.acl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 16:00
 */
@Component
public class UserAcl {

    private static final Logger logger = LoggerFactory.getLogger(UserAcl.class);

//    @Autowired
//    private FeignClient feignClient;

    public void SendMsg(String info) {

//        feignClient.sendMsg(info);
        logger.info("外部调用，入参是{}，返回结果是，{}", info, "");
    }
}
