package com.meta.ddd.service;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.meta.ddd.dto.UserDTO;
import com.meta.ddd.entity.UserEntity;
import com.meta.ddd.param.UserParam;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 14:51
 * <p>
 * Service层
 */
@Service
public class UserService implements IService<UserParam> {

    @Override
    public UserDTO add(UserParam param) {

        //构建模型 添加用户 发送短信
        UserEntity userEntity = new UserEntity(param.getGender(), param.getMobile(), param.getEmail());
        //todo userEntity-> UserDTO   可以用mapstruct来转换
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userEntity, userDTO);
        return userDTO;
    }

    @Override
    public UserDTO delete(UserParam param) {
        return null;
    }

    @Override
    public UserDTO query(UserParam param) {
        return null;
    }

    @Override
    public UserDTO update(UserParam param) {
        return null;
    }
}
