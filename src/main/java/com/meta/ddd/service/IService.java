package com.meta.ddd.service;

import com.meta.ddd.dto.IDTO;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 14:51
 */
public interface IService<IParam> {

    /**
     * 新增
     *
     * @param t
     * @return
     */
    IDTO add(IParam t);

    /**
     * 删除
     *
     * @param t
     * @return
     */
    IDTO delete(IParam t);

    /**
     * 查询
     *
     * @param t
     * @return
     */
    IDTO query(IParam t);

    /**
     * 修改
     *
     * @param t
     * @return
     */
    IDTO update(IParam t);
}
