package com.meta.ddd.repository;

import com.meta.ddd.dto.IDTO;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 14:56
 */
public interface IRepository<T> {

    /**
     * 新增
     *
     * @param t
     * @return
     */
    IDTO add(T t);

    /**
     * 删除
     *
     * @param t
     * @return
     */
    IDTO delete(T t);

    /**
     * 查询
     *
     * @param t
     * @return
     */
    IDTO query(T t);

    /**
     * 修改
     *
     * @param t
     * @return
     */
    IDTO update(T t);
}
