package com.meta.ddd.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.meta.ddd.dao.UserDao;
import com.meta.ddd.dto.UserDTO;
import com.meta.ddd.entity.UserEntity;
import com.meta.ddd.po.UserPO;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 14:56
 * <p>
 * 仓储层
 */
@Repository
public class UserRepository implements IRepository<UserEntity> {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDTO add(UserEntity userEntity) {

        // todo UserEntity->UserPO   引入mapStrut工具来进行转换
        UserPO add = userDao.add(new UserPO());
        // todo UserPO->UserDTO     引入mapStrut工具来进行转换
        return new UserDTO();
    }

    @Override
    public UserDTO delete(UserEntity userEntity) {
        return null;
    }

    @Override
    public UserDTO query(UserEntity userEntity) {
        return null;
    }

    @Override
    public UserDTO update(UserEntity userEntity) {
        return null;
    }

    public UserDTO queryByMobile(String mobile) {

        return new UserDTO();
    }

    public UserDTO queryByMobileNull(String mobile) {

        return null;
    }
}
