package com.meta.ddd.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meta.common.vo.ResponseInfo;
import com.meta.ddd.param.UserParam;
import com.meta.ddd.service.UserService;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 14:51
 */
@RestController("/user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @PostMapping("/add")
    public ResponseInfo add(UserParam param) {

        logger.info("关键点可能需要一点日志，{}");
        return ResponseInfo.succ(userService.add(param));
    }
}
