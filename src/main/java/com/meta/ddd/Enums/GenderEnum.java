package com.meta.ddd.Enums;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 15:05
 * <p>
 * 男女枚举类
 */
public enum GenderEnum {

    Boy(1, "男"),
    Girl(0, "女");

    private Integer id;
    private String desc;

    GenderEnum(Integer id, String desc) {
        this.id = id;
        this.desc = desc;
    }

    /**
     * 数据库里面存Id,页面上描述从这里获取
     *
     * @param id
     * @return
     */
    public static String getDescById(Integer id) {

        if (null == id) {
            return Girl.desc;
        }
        GenderEnum[] values = GenderEnum.values();
        for (GenderEnum value : values) {
            if (id.equals(value.id)) {
                return value.desc;
            }
        }
        return Girl.desc;
    }

    /**
     * 根据Id获取枚举值
     *
     * @param id
     * @return
     */
    public static GenderEnum getById(Integer id) {

        if (null == id) {
            return GenderEnum.Girl;
        }
        GenderEnum[] values = GenderEnum.values();
        for (GenderEnum value : values) {
            if (id.equals(value.id)) {
                return value;
            }
        }
        return GenderEnum.Girl;
    }
}
