package com.meta.ddd.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 15:29
 */
@Setter
@Getter
public class UserDTO extends IDTO {

    private String id;
    private Integer gender;
    private String mobile;
    private String email;

    @Override
    public String toString() {
        return "UserDTO{" +
                "id='" + id + '\'' +
                ", gender=" + gender +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
