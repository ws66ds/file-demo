package com.meta.ddd.feign;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import com.meta.common.vo.ResponseInfo;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 16:21
 */
@Component
public interface FeignClient {

    /**
     * 发送短信
     * @param into
     * @return
     */
    @PostMapping()
    ResponseInfo sendMsg(@Param("info") String into);
}
