package com.meta.ddd.po;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * @Author: xieZW
 * @Date: 2022/1/19 15:20
 * <p>
 * todo
 */
@Getter
@Setter
@Table()
public class UserPO {

    @Column
    private String id;
    @Column
    private Integer gender;
    @Column
    private String mobile;
    @Column
    private String email;
}
