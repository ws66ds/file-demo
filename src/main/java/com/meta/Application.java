package com.meta;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.meta.common.utils.RedisConfig;

/**
 * @Author: xieZW
 * @Date: 2021/11/5 13:28
 */
@EnableCaching
@EnableTransactionManagement
@ServletComponentScan
@SpringBootApplication
@MapperScan(basePackages = {"com.meta.api.mapper", "com.meta.app.mapper"})
@Import(RedisConfig.class)
@EnableScheduling
public class Application {

//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(Application.class);
//    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
