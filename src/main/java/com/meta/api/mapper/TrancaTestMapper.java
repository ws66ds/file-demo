package com.meta.api.mapper;

import org.springframework.transaction.annotation.Transactional;

import com.meta.api.po.UserModel;

/**
 * @Author: xieZW
 * @Date: 2021/12/31 13:54
 */
@Transactional
public interface TrancaTestMapper {

    //    @Transactional
    void updateTest01(UserModel userModel);

    //    @Transactional
    void updateTest02(UserModel userModel);
}
