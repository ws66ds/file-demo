package com.meta.api.mapper;


import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.meta.api.po.UserModel;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2021/11/9 15:23
 */
@Component
public interface UserMapper {

    List<UserModel> selectByNameAndPwd(@Param("username") String name, @Param("password") String password);

    int insert(UserModel userModel);

    int insertTemp(UserModel userModel);

    int delete(UserModel userModel);

    List<UserModel> selectAll();

    List<UserModel> selectByLimit(@Param("username") String userName, @Param("mobile") String userMobile, @Param("start") Integer start, @Param("limit") Integer limit);

    List<UserModel> selectByMobile(@Param("mobile") String mobile);

    /**
     * 更新
     *
     * @param userModel return int
     *                   * @return
     */
    int update(UserModel userModel);

    /**
     * 更新
     *
     * @param userModel return int
     *                  * @return
     */
    int updateTest(UserModel userModel);

    /**
     * @param userId
     * @return
     */
    int pwd(@Param("userId") String userId);
}
