package com.meta.api.service.propagationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.meta.api.mapper.UserMapper;
import com.meta.api.param.UserNamePwdParam;
import com.meta.api.po.UserModel;
import com.meta.common.enums.CommonConstants;
import java.sql.Timestamp;

/**
 * @Author: xieZW
 * @Date: 2021/11/17 16:46
 * <p>
 * 测试传播特性
 */
@Component
public class TrancaPropagationB {

    @Autowired
    private UserMapper userDao;

    static UserModel userModel = new UserModel();

    static {
        userModel.setUserId(1L);
    }


    @Transactional(rollbackFor = Exception.class, propagation = Propagation.NOT_SUPPORTED)
    public void methodB(UserNamePwdParam param) {

        UserModel userModel = new UserModel();
        userModel.setPassword(param.getPassword());
        userModel.setUserId(2L);
        userModel.setUsername(param.getUsername());
        userModel.setEmail(param.getEmail() == null ? "122" : param.getEmail());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        userModel.setAddDate(timestamp);
        userModel.setStatus(CommonConstants.Status.NORMAL);
        userModel.setUpdateDate(timestamp);
        int insert = userDao.insertTemp(userModel);
        System.out.println("insertInfo = " + insert);
        throw new RuntimeException("我是B,要回滚了，A你不跟着回滚吗！");
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.NOT_SUPPORTED)
    public void methodB02(UserNamePwdParam param) {

        UserModel userModel = new UserModel();
        userModel.setPassword(param.getPassword());
        userModel.setUserId(2L);
        userModel.setUsername(param.getUsername());
        userModel.setEmail(param.getEmail() == null ? "122" : param.getEmail());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        userModel.setAddDate(timestamp);
        userModel.setStatus(CommonConstants.Status.NORMAL);
        userModel.setUpdateDate(timestamp);
        int insert = userDao.insertTemp(userModel);
        System.out.println("insertInfo = " + insert);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.NEVER)
    public void methodB03(UserNamePwdParam param) {
        System.out.println("直接报错吧");
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.MANDATORY)
    public void methodB04(UserNamePwdParam param) {
        System.out.println("直接报错吧");
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void methodB05(UserNamePwdParam param) {

        UserModel userModel = new UserModel();
        userModel.setPassword(param.getPassword());
        userModel.setUserId(2L);
        userModel.setUsername(param.getUsername());
        userModel.setEmail(param.getEmail() == null ? "122" : param.getEmail());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        userModel.setAddDate(timestamp);
        userModel.setStatus(CommonConstants.Status.NORMAL);
        userModel.setUpdateDate(timestamp);
        int insert = userDao.insertTemp(userModel);
        System.out.println("insertInfo = " + insert);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void methodB06(UserNamePwdParam param) {

        UserModel userModel = new UserModel();
        userModel.setPassword(param.getPassword());
        userModel.setUserId(2L);
        userModel.setUsername(param.getUsername());
        userModel.setEmail(param.getEmail() == null ? "122" : param.getEmail());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        userModel.setAddDate(timestamp);
        userModel.setStatus(CommonConstants.Status.NORMAL);
        userModel.setUpdateDate(timestamp);
        int insert = userDao.insertTemp(userModel);
        System.out.println("insertInfo = " + insert);
        throw new RuntimeException("我是B,我报错插入失败，A删除失败！");

    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.SUPPORTS)
    public void methodB07(UserNamePwdParam param) {

        UserModel userModel = new UserModel();
        userModel.setPassword(param.getPassword());
        userModel.setUserId(2L);
        userModel.setUsername(param.getUsername());
        userModel.setEmail(param.getEmail() == null ? "122" : param.getEmail());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        userModel.setAddDate(timestamp);
        userModel.setStatus(CommonConstants.Status.NORMAL);
        userModel.setUpdateDate(timestamp);
        int insert = userDao.insertTemp(userModel);
        System.out.println("insertInfo = " + insert);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void methodB09(UserNamePwdParam param) {
        UserModel userModel = new UserModel();
        userModel.setPassword(param.getPassword());
        userModel.setUserId(2L);
        userModel.setUsername(param.getUsername());
        userModel.setEmail(param.getEmail() == null ? "122" : param.getEmail());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        userModel.setAddDate(timestamp);
        userModel.setStatus(CommonConstants.Status.NORMAL);
        userModel.setUpdateDate(timestamp);
        int insert = userDao.insert(userModel);
        System.out.println("insertInfo = " + insert);
    }


    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void sleepTwo(Integer num) throws InterruptedException {

        this.userDao.updateTest(userModel);
        Thread.sleep(2000);
        if (num == 8) {
            throw new RuntimeException();
        }
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void deleteUser() {
        this.userDao.updateTest(userModel);
    }
}
