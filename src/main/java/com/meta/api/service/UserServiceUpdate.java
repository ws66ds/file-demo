package com.meta.api.service;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.meta.api.mapper.UserMapper;
import com.meta.api.param.UserNamePwdParam;
import com.meta.api.param.UserParam;
import com.meta.api.po.UserModel;
import com.meta.common.enums.CommonConstants;
import com.meta.common.utils.DateUtil;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: xieZW
 * @Date: 2021/11/16 17:06
 * 更新
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceUpdate {

    @Autowired
    private UserMapper userDao;
    @Autowired
    private UserServiceQuery userServiceQuery;

    public UserModel register(UserNamePwdParam param) {

        List<UserModel> userList = userDao.selectByNameAndPwd(param.getUsername(), param.getPassword());
        if (CollectionUtils.isNotEmpty(userList)) {
            throw new RuntimeException("用户已存在！");
        }
        UserModel userModel = new UserModel();
        userModel.setUsername(param.getUsername());
        userModel.setPassword(param.getPassword());
        userModel.setEmail(param.getEmail() == null ? "122" : param.getEmail());

        Timestamp addDate = DateUtil.mysqlTimestamp();
        userModel.setAddDate(addDate);
        userModel.setUpdateDate(addDate);
        userModel.setStatus(CommonConstants.Status.NORMAL);
        int insert = userDao.insert(userModel);
        if (insert <= 0) {
            throw new RuntimeException("用户注册失败！");
        }
        return userModel;
    }

    public void update(UserNamePwdParam param) {

        this.userServiceQuery.existUser(param);
        UserModel userModel = new UserModel();
        userModel.setUserId(1L);
        this.userDao.delete(userModel);
        this.register(param);
        this.userServiceQuery.existUserEx(param);
    }

    public void updateThread(UserNamePwdParam param) {

        this.userServiceQuery.existUser(param);
        final UserModel userModel = new UserModel();

        userModel.setUserId(1L);


        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                userDao.delete(userModel);
            }
        });
        this.register(param);
        this.userServiceQuery.existUserEx(param);
    }

    public UserModel save(UserParam param) {

        UserModel userModel = new UserModel();
        if (StringUtils.isEmpty(param.getUserId())) {
            //新增
            userModel.setUsername(param.getUsername());
            userModel.setPassword("123456");
            userModel.setMobile(param.getMobile());
            userModel.setEmail(param.getEmail() == null ? "122" : param.getEmail());
            Timestamp addDate = DateUtil.mysqlTimestamp();
            userModel.setAddDate(addDate);
            userModel.setUpdateDate(addDate);
            userModel.setStatus(CommonConstants.Status.NORMAL);
            userDao.insert(userModel);
        } else {
            //修改
            userModel.setUserId(Long.valueOf(param.getUserId()));
            userModel.setUsername(param.getUsername());
            userModel.setMobile(param.getMobile());
            userModel.setMoney(param.getMoney());
            userModel.setEmail(param.getEmail() == null ? "122" : param.getEmail());
            Timestamp addDate = DateUtil.mysqlTimestamp();
            userModel.setUpdateDate(addDate);
            userModel.setStatus(CommonConstants.Status.NORMAL);
            userDao.update(userModel);
        }
        return userModel;
    }

    public Integer delete(String ids) {
        UserModel userModel = new UserModel();
        userModel.setUserId(Long.valueOf(ids));
        int delete = userDao.delete(userModel);
        return delete;
    }

    public Integer pwd(UserParam param) {

        userDao.pwd(param.getUserId());
        return null;
    }
}
