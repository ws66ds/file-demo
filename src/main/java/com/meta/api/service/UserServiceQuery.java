package com.meta.api.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.meta.api.mapper.UserMapper;
import com.meta.api.param.UserNamePwdParam;
import com.meta.api.param.UserParam;
import com.meta.api.po.UserModel;
import com.meta.app.entity.MemberEntity;
import com.meta.common.vo.PageInfoVO;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2021/11/9 15:22
 */
@Service
@Transactional(rollbackFor = Exception.class, readOnly = true)
public class UserServiceQuery {


    @Autowired
    private UserMapper userDao;

    public UserModel login(UserNamePwdParam param) {

        List<UserModel> userList = userDao.selectByNameAndPwd(param.getUsername(), param.getPassword());
        if (CollectionUtils.isEmpty(userList) || userList.size() > 1) {
            throw new RuntimeException("用户名或密码错误！");
        }
        return userList.get(0);
    }

    /**
     * 用户是否存在
     *
     * @param param
     * @return
     */
    public boolean existUser(UserNamePwdParam param) {

        List<UserModel> userModels = userDao.selectByNameAndPwd(param.getUsername(), param.getPassword());
//        throw new RuntimeException("啊 我出错了！");
        return CollectionUtils.isEmpty(userModels);
    }

    /**
     * 用户是否存在
     * 报错
     *
     * @param param
     * @return
     */
    public boolean existUserEx(UserNamePwdParam param) {

        List<UserModel> userModels = userDao.selectByNameAndPwd(param.getUsername(), param.getPassword());

        throw new RuntimeException("数据会删除，新增会被回滚！");
    }

    public PageInfoVO<List<UserModel>> queryUserList(UserParam param) {

        PageInfoVO pageInfoVO = new PageInfoVO();
        int start = (param.getPage() - 1) * param.getLimit();
        List<UserModel> userModels = userDao.selectByLimit(param.getUsername(), param.getMobile(), start, param.getLimit());
        List<UserModel> all = userDao.selectByLimit(param.getUsername(), param.getMobile(), null, null);
        int size = all.size();
        pageInfoVO.setCount(size);
        pageInfoVO.setData(userModels);
        return pageInfoVO;
    }

    public UserModel wx_login(MemberEntity param) {

        //先判断下是否存在

        //存在，登录
        //不存在，注册
        return null;
    }
}
