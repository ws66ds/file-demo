package com.meta.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.meta.api.mapper.UserMapper;
import com.meta.api.param.UserNamePwdParam;
import com.meta.api.po.UserModel;

/**
 * @Author: xieZW
 * @Date: 2021/11/17 15:09
 */
@Component
public class TransaInMethod {

    @Autowired
    private UserMapper userDao;

    // ------------------------------------------------------------------------------------------- //

    public void updateANoBYes(UserNamePwdParam param) {

        methodANo(param);
    }

    public void methodANo(UserNamePwdParam param) {

        UserModel userModel = new UserModel();
        userModel.setUserId(1L);
        this.userDao.delete(userModel);
        methodBYes(param);
    }


    @Transactional(rollbackFor = Exception.class)
    public void methodBYes(UserNamePwdParam param) {

        throw new RuntimeException("我是方法B，我要报错了，A你照样删除数据");
    }

    // ------------------------------------------------------------------------------------------- //

    public void updateAYesBNo(UserNamePwdParam param) {
        methodAYes(param);
    }

    @Transactional(rollbackFor = Exception.class)
    public void methodAYes(UserNamePwdParam param) {

        UserModel userModel = new UserModel();
        userModel.setUserId(1L);
        this.userDao.delete(userModel);
        methodBNo(param);
    }

    public void methodBNo(UserNamePwdParam param) {
        throw new RuntimeException("我是方法B，我要报错了，A你照样删除数据");
    }


    // ------------------------------------------------------------------------------------------- //


    public void updateAYesBYes(UserNamePwdParam param) {

        methodAYes02(param);
    }

    @Transactional(rollbackFor = Exception.class)
    public void methodAYes02(UserNamePwdParam param) {

        UserModel userModel = new UserModel();
        userModel.setUserId(1L);
        this.userDao.delete(userModel);
        methodBYes(param);
    }

    // ------------------------------------------------------------------------------------------- //

    @Transactional(rollbackFor = Exception.class)
    public void updateAYesBYesThisYes(UserNamePwdParam param) {
        methodAYes02(param);
    }


    // ------------------------------------------------------------------------------------------- //


    @Transactional(rollbackFor = Exception.class)
    public void updateANoBNoThisYes(UserNamePwdParam param) {

        methodANo02(param);
    }

    private void methodANo02(UserNamePwdParam param) {

        methodBNo(param);
    }
}
