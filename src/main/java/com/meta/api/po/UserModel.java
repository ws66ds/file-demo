package com.meta.api.po;

import lombok.Data;

import com.meta.common.vo.Token;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * @Author: xieZW
 * @Date: 2021/11/9 15:36
 */
@Data
@Table(name = "t_user")
@Entity
public class UserModel extends Token {

    private static final long serialVersionUID = -7112239901056819693L;

    @Id
    @Column(name = "c_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long userId;


    @Column(name = "C_USER_NAME")
    private String username;

    @Column(name = "C_MONEY")
    private String money;

    @Column(name = "C_PASSWORD")
    private String            password;

    @Column(name = "C_EMAIL")
    private String            email;

    @Column(name = "C_MOBILE")
    private String mobile;

    @Column(name = "C_ADD_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addDate;

    @Column(name = "C_UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date              updateDate;

    @Column(name = "C_STATUS")
    private Integer           status;

}
