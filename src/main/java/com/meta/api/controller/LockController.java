package com.meta.api.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meta.api.service.propagationService.TrancaPropagationA;
import java.util.concurrent.CountDownLatch;

/**
 * @Author: xieZW
 * @Date: 2021/12/16 15:30
 */
@RestController
@RequestMapping("lock")
@Slf4j
public class LockController {

    @Autowired
    private TrancaPropagationA trancaPropagationA;

    @GetMapping("/start")
    public void UserInfo() {

        final CountDownLatch latch = new CountDownLatch(40);

        for (int i = 0; i < 40; i++) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    latch.countDown();
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Thread.currentThread().getName() = " + Thread.currentThread().getName());
                    trancaPropagationA.update14(null);
                }
            };
            thread.start();
        }
        System.out.println(" the end ------------------");
    }

    @GetMapping("/end")
    public void UserInfo2() {

        final CountDownLatch latch = new CountDownLatch(40);

        for (int i = 0; i < 40; i++) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    latch.countDown();
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Thread.currentThread().getName() = " + Thread.currentThread().getName());
                    trancaPropagationA.update15(null);
                }
            };
            thread.start();
        }

        System.out.println("----------------------------- the end ");
    }


    @GetMapping("/sleep")
    public void UserInfo3() {

        trancaPropagationA.deleteUser22();
        System.out.println("----------------------------- the end ");
    }
}
