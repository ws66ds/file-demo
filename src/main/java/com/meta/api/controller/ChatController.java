package com.meta.api.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.meta.app.entity.MemberEntity;
import com.meta.app.entity.chat.ChatGptEntity;
import com.meta.app.entity.chat.ChoicesEntity;
import com.meta.auth.utils.StringUtil;
import com.meta.common.HttpUtils;
import com.meta.common.vo.PageInfoVO;
import com.meta.common.vo.ResponseInfo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: xieZW
 * @Date: 2023/2/13 14:21
 */
@RestController
@RequestMapping("api/appChatGPT")
@Slf4j
public class ChatController {

    @PostMapping("/list")
    public ResponseInfo UserInfo(@RequestBody MemberEntity param) {

        log.info("查询数据");

        if (StringUtil.isEmpty(param.getNickName())) {
            return ResponseInfo.error("输入问题不能为空！");
        }

        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        header.put("Authorization", "Bearer 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");

        HashMap<Object, Object> paramsss = new HashMap<>();
        paramsss.put("model", "text-davinci-003");
        paramsss.put("best_of", 1);
        paramsss.put("echo", true);
        paramsss.put("frequency_penalty", 0);
        paramsss.put("logprobs", 0);
        paramsss.put("max_tokens", 256);
        paramsss.put("presence_penalty", 0);
        paramsss.put("prompt", param.getNickName());
        paramsss.put("temperature", 0.7);
        paramsss.put("top_p", 1);
        String s = null;
        try {
            s = HttpUtils.httpPostToJson("https://api.openai.com/v1/completions", paramsss, header);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("s = " + s);
        JSONObject jsonObject = JSONObject.parseObject(s);
        ChatGptEntity chatGptEntity = JSON.toJavaObject(jsonObject, ChatGptEntity.class);
        List<ChoicesEntity> info = chatGptEntity.getChoices();

        List<MemberEntity> userModels = new ArrayList<>();
        MemberEntity memberEntity = new MemberEntity();
        if (info != null && info.size() > 0) {
            for (int i = 0; i < info.size(); i++) {
                ChoicesEntity o = info.get(i);
                String text = o.getText();
                String[] split1 = text.split(param.getNickName());
//                String[] split = text.split("\n\n");
//                split[0] = null;
//                int length = split.length;
//                StringBuffer stringBuffer = new StringBuffer();
//                for (int i1 = 1; i1 < length; i1++) {
//                    stringBuffer.append(i1).append("\n\n");
//                }
                String s1 = split1[1];
                memberEntity.setNickName(param.getNickName());
                memberEntity.setChatResult(s1);
                System.out.println("s1 = " + s1);
            }
        }
        userModels.add(memberEntity);

        PageInfoVO pageInfoVO = new PageInfoVO();
        pageInfoVO.setCount(1);
        pageInfoVO.setData(userModels);

        return ResponseInfo.succ(userModels);
    }
}
