package com.meta.api.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meta.app.entity.RecordMoneyEntity;
import com.meta.app.service.RecordMoneyService;
import com.meta.auth.ShiroUtils;
import com.meta.auth.vo.CorpUserInfoVO;
import com.meta.common.vo.ResponseInfo;
import java.util.List;

/**
 * @Author: xieZW
 * @Date: 2022/1/27 11:07
 */
@RestController
@RequestMapping("api/money")
@Slf4j
public class MoneyController {

    @Autowired
    private RecordMoneyService recordMoneyService;

    @PostMapping("queryRecord")
    public ResponseInfo query(@RequestBody RecordMoneyEntity entity) {

        List<RecordMoneyEntity> recordMoneyEntities = recordMoneyService.queryRecord(entity.getUserId());
        return ResponseInfo.succ(recordMoneyEntities);
    }
}
