package com.meta.api.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meta.api.param.UserParam;
import com.meta.api.service.UserServiceQuery;
import com.meta.api.service.UserServiceUpdate;
import com.meta.app.entity.MemberEntity;
import com.meta.app.service.MemberService;
import com.meta.common.pemi.Pemi02;
import com.meta.common.vo.PageInfoVO;
import com.meta.common.vo.ResponseInfo;

/**
 * @Author: xieZW
 * @Date: 2021/11/19 22:37
 */
@RestController
@RequestMapping("api/appUser")
@Slf4j
public class AppUserController {


    @Autowired
    private UserServiceQuery userServiceQuery;

    @Autowired
    private MemberService memberService;

    @Autowired
    private UserServiceUpdate userServiceUpdate;


    @PostMapping("/list")
    public ResponseInfo UserInfo(@RequestBody MemberEntity param) {

        log.info("查询数据");
        PageInfoVO userModels = memberService.queryUserList(param);

        return ResponseInfo.succ(userModels);
    }


    @PostMapping("/save")
    @Pemi02
    public ResponseInfo save(@RequestBody MemberEntity param) {

        if (null == param.getId()) {
            memberService.addUser(param);
        } else {
            memberService.updateUser(param);
        }
        return ResponseInfo.succ();
    }


    @GetMapping("/delete")
    @Pemi02
    public ResponseInfo delete(@Param("ids") String ids) {

        return ResponseInfo.succ(userServiceUpdate.delete(ids));
    }

    @PostMapping("/pwd")
    @Pemi02
    public ResponseInfo pwd(@RequestBody UserParam param) {

        return ResponseInfo.succ(userServiceUpdate.pwd(param));
    }
}
