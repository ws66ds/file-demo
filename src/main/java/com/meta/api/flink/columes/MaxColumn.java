package com.meta.api.flink.columes;

import org.apache.flink.table.functions.ScalarFunction;

/**
 * 自定义单行函数:MAXCOLUMN
 */
public class MaxColumn extends ScalarFunction {

    //算子名称
    public static final String MAXCOLUMN = "MAXCOLUMN";

    public MaxColumn() {
        super();
    }

    /**
     * 最大值计算单行函数:MAXCOLUMN
     *
     * @param rows 每行的若干列(Double)
     * @return 返回多列中最大值
     */
    public Double eval(Double... rows) {
        Double max = null;
        for (Double row : rows) {
            if (max == null || row > max) {
                max = row;
            }
        }
        max = max == null ? 0f : max;
        return max;
    }
}
