package com.meta.api.flink.columes;

import org.apache.flink.table.functions.ScalarFunction;

/**
 * 自定义单行函数:MINCOLUMN
 */
public class MinColumn extends ScalarFunction {

    //算子名称
    public static final String MINCOLUMN = "MINCOLUMN";

    public MinColumn() {
        super();
    }

    /**
     * 最大值计算单行函数:MINCOLUMN
     *
     * @param rows 每行的若干列(Double)
     * @return 返回多列中最小值
     */
    public Double eval(Double... rows) {
        Double min = null;
        for (Double row : rows) {
            if (min == null || row < min) {
                min = row;
            }
        }
        min = min == null ? 0f : min;
        return min;
    }
}
