package com.meta.api.flink.columes;

import org.apache.flink.table.functions.ScalarFunction;

import java.sql.Timestamp;

/**
 * 自定义单行函数:UNIX_MILLIS
 */
public class UnixMillisColumn extends ScalarFunction {

    //算子名称
    public static final String UNIX_MILLIS = "UNIX_MILLIS";

    public UnixMillisColumn() {
        super();
    }

    /**
     * 获取时间戳long值:UNIX_MILLIS
     *
     * @param timestamp 时间戳
     * @return 时间戳对应long值
     */
    public Long eval(Timestamp timestamp) {
        return timestamp.getTime();
    }
}
