package com.meta.api.flink.columes;

import org.apache.flink.table.functions.ScalarFunction;

public class AmplitudeColumn extends ScalarFunction {

    //算子名称
    public static final String AMPLITUDECOLUMN = "AMPLITUDECOLUMN";

    public AmplitudeColumn() {
        super();
    }

    /**
     * 幅度计算单行函数:AMPLITUDECOLUMN
     *
     * @param cols 每行的若干列(Double)
     * @return 返回多列中最大值与最小值的差
     */
    public Double eval(Double... cols) {
        Double max = null, min = null;
        for (Double col : cols) {
            if (max == null || col > max) {
                max = col;
            }
            if (min == null || col < min) {
                min = col;
            }
        }
        max = max == null ? 0f : max;
        min = min == null ? 0f : min;
        return max - min;
    }
}
