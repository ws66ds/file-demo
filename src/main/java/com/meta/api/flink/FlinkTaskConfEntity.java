package com.meta.api.flink;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

//flink任务详细配置
public class FlinkTaskConfEntity {

    //任务并行度
    private int parallelism = 1;

    /////////////////////////////////////////////
    //服务配置
    private Map<String, String> serviceMap;
    //分组字段
    private Map<String, String> groups;
    //输出信息
    private Map<String, String> alertMap;
    //输入配置
    private Map<String, String> inputMap;

    public int getParallelism() {
        return parallelism;
    }

    public void setParallelism(int parallelism) {
        this.parallelism = parallelism;
    }

    public Map<String, String> getServiceMap() {
        return serviceMap;
    }

    public void setServiceMap(Map<String, String> serviceMap) {
        this.serviceMap = serviceMap;
    }

    public Map<String, String> getGroups() {
        return groups;
    }

    public void setGroups(Map<String, String> groups) {
        this.groups = groups;
    }

    public Map<String, String> getAlertMap() {
        return alertMap;
    }

    public void setAlertMap(Map<String, String> alertMap) {
        this.alertMap = alertMap;
    }

    public Map<String, String> getInputMap() {
        return inputMap;
    }

    public void setInputMap(Map<String, String> inputMap) {
        this.inputMap = inputMap;
    }

    public Map<String, String> getOutputMap() {
        return outputMap;
    }

    public void setOutputMap(Map<String, String> outputMap) {
        this.outputMap = outputMap;
    }

    public Map<String, String> getRuleMap() {
        return ruleMap;
    }

    public void setRuleMap(Map<String, String> ruleMap) {
        this.ruleMap = ruleMap;
    }

    public Map<String, String> getSystemMap() {
        return systemMap;
    }

    public void setSystemMap(Map<String, String> systemMap) {
        this.systemMap = systemMap;
    }

    public Map<String, String> getFlinkConf() {
        return flinkConf;
    }

    public void setFlinkConf(Map<String, String> flinkConf) {
        this.flinkConf = flinkConf;
    }

    public List<String> getKafkaList() {
        return kafkaList;
    }

    public void setKafkaList(List<String> kafkaList) {
        this.kafkaList = kafkaList;
    }

    public List<String> getHbaseList() {
        return hbaseList;
    }

    public void setHbaseList(List<String> hbaseList) {
        this.hbaseList = hbaseList;
    }

    public Map<String, Map<String, String>> getOutputInfoMap() {
        return outputInfoMap;
    }

    public void setOutputInfoMap(Map<String, Map<String, String>> outputInfoMap) {
        this.outputInfoMap = outputInfoMap;
    }

    public List<Map<String, String>> getRuleList() {
        return ruleList;
    }

    public void setRuleList(List<Map<String, String>> ruleList) {
        this.ruleList = ruleList;
    }

    public String getRuleRuleId() {
        return ruleRuleId;
    }

    public void setRuleRuleId(String ruleRuleId) {
        this.ruleRuleId = ruleRuleId;
    }

    public Map<String, Object> getRuleAlertCol() {
        return ruleAlertCol;
    }

    public void setRuleAlertCol(Map<String, Object> ruleAlertCol) {
        this.ruleAlertCol = ruleAlertCol;
    }

    public Map<String, String> getRuleRuleInfo() {
        return ruleRuleInfo;
    }

    public void setRuleRuleInfo(Map<String, String> ruleRuleInfo) {
        this.ruleRuleInfo = ruleRuleInfo;
    }

    public String getRuleServiceRuleId() {
        return ruleServiceRuleId;
    }

    public void setRuleServiceRuleId(String ruleServiceRuleId) {
        this.ruleServiceRuleId = ruleServiceRuleId;
    }

    public String[] getRuleFieldNames() {
        return ruleFieldNames;
    }

    public void setRuleFieldNames(String[] ruleFieldNames) {
        this.ruleFieldNames = ruleFieldNames;
    }

    public List<String> getGroupAttrIds() {
        return groupAttrIds;
    }

    public void setGroupAttrIds(List<String> groupAttrIds) {
        this.groupAttrIds = groupAttrIds;
    }

    //输出配置
    private Map<String, String> outputMap;
    //规则列表
    private Map<String, String> ruleMap;
    //系统配置
    private Map<String, String> systemMap;
    //flink配置
    private Map<String, String> flinkConf;
    //输入节点键值集合
    private List<String> kafkaList;
    //输入节点键值集合
    private List<String> hbaseList;
    //输出配置节点
    private Map<String, Map<String, String>> outputInfoMap;
    //规则配置列表
    private List<Map<String, String>> ruleList;

    /////////////////////////////////////////////
    //规则id
    private String ruleRuleId = null;
    //预警列
    private Map<String, Object> ruleAlertCol = new HashMap<>();
    //规则配置
    private Map<String, String> ruleRuleInfo = new HashMap<>();
    //服务规则标识
    private String ruleServiceRuleId = null;
    //规则输出结构
    private String[] ruleFieldNames = null;
    //输入数据分组字段
    private List<String> groupAttrIds = null;
}
