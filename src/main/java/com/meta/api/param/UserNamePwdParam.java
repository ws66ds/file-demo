package com.meta.api.param;

import lombok.Data;

import com.meta.param.IParam;

/**
 * @Author: xieZW
 * @Date: 2021/11/9 15:08
 */
@Data
public class UserNamePwdParam extends IParam {

    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 验证码
     */
    private String code;

    /**
     * 邮箱
     */
    private String email;

    public UserNamePwdParam(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserNamePwdParam() {
    }
}
