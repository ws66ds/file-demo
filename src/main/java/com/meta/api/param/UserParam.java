package com.meta.api.param;

import lombok.Data;

import com.meta.param.IParam;

/**
 * @Author: xieZW
 * @Date: 2021/11/19 22:40
 */
@Data
public class UserParam extends IParam {

    /**
     * 主键
     */
    private String userId;
    private String deptId;
    private String username;
    private String money;
    private String mobile;
    private String email;
    private String isLock;
}
