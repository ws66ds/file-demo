package com.meta.api.param;

import lombok.Data;

import com.meta.param.IParam;

/**
 * @Author: xieZW
 * @Date: 2021/11/25 18:39
 * <p>
 * 微信用户
 */
@Data
public class WXUserParam extends IParam {

    private String nickName;
    private String logo;
    private String city;
    private String province;
    private String encryptedData;
    private String iv;
    private String sign;
    private String code;
    private String fromId;
    private String cpId;
}
